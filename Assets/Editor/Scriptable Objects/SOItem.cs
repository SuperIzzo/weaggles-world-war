﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SOItem
{
	[MenuItem("Assets/Create/Item/Dropper")]
	public static void CreateDropperAsset ()
	{
		ScriptableObjectUtility.CreateAsset<Dropper> ();
	}
}
