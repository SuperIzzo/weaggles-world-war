﻿using UnityEngine;
using UnityEditor;
using System.IO;

public static class ScriptableObjectUtility
{
    /// <summary>
    //	This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    public static void CreateAsset<T>() where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T> ();

        string path = AssetDatabase.GetAssetPath (Selection.activeObject);
        if( path == "" )
        {
            path = "Assets";
        }
        else if( Path.GetExtension( path ) != "" )
        {
            path = path.Replace( Path.GetFileName( AssetDatabase.GetAssetPath( Selection.activeObject ) ), "" );
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset( asset, assetPathAndName );

        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }


    [MenuItem( "Assets/Create/ScriptableObject" )]
    public static void CreateScriptableObjectAsset()
    {
        string fullPath = AssetDatabase.GetAssetPath( Selection.activeObject );

        if( fullPath == "" || Path.GetExtension( fullPath ) == "" )
        {
            throw new UnityException( "No file selected." );
        }
        else
        {
            string outputPath = "";
            string fileName = Path.GetFileName( fullPath );
            string className = Path.GetFileNameWithoutExtension( fileName );

            outputPath = fullPath.Replace( fileName, "" );

            var scriptableObject = ScriptableObject.CreateInstance(className);

            if( !scriptableObject )
            {
                throw new UnityException("Failed to create ScriptableObject \"" + className + "\". Make sure the class inherits ScriptableObject and the class name matches the filename.");
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(outputPath + "/" + className + ".asset");

            AssetDatabase.CreateAsset( scriptableObject, assetPathAndName );

            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = scriptableObject;
        }
    }

    [MenuItem( "Assets/Create/ScriptableObject", true )]
    public static bool ValidateCreateScriptableObjectAsset()
    {
        string fullPath = AssetDatabase.GetAssetPath( Selection.activeObject );

        return Path.GetExtension(fullPath) == ".cs";
    }
}