﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEditor;

    [CustomEditor( typeof( Health ) )]
    public class HealthInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty currentProp  = serializedObject.FindProperty( "_current" );
            SerializedProperty maxProp      = serializedObject.FindProperty( "_max" );

            GUILayoutOption[] floatFieldOptions = new GUILayoutOption[1];
            floatFieldOptions[0] = GUILayout.MaxWidth( 40 );

            GUILayoutOption[] separatorOptions = new GUILayoutOption[1];
            separatorOptions[0] = GUILayout.MaxWidth( 10 );

            if( Screen.width < 333 )
            {
                EditorGUILayout.PrefixLabel( "Health" );
                EditorGUILayout.BeginHorizontal();
            }
            else
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel( "Health" );
            }

            // Current health as a slider
            currentProp.floatValue =
            EditorGUILayout.Slider( currentProp.floatValue, 0, maxProp.floatValue );

            // Separator
            EditorGUILayout.LabelField( "/", separatorOptions );

            // Max health
            maxProp.floatValue = EditorGUILayout.FloatField( maxProp.floatValue, floatFieldOptions );

            // End drawing
            EditorGUILayout.EndHorizontal();
            

            serializedObject.ApplyModifiedProperties();
        }
    }
}