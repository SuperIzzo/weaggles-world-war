﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;

    [RequireComponent( typeof( Collider ) )]
    [AddComponentMenu( "Health/Health Collider Affector", 20 )]
    public class HealthColliderAffector : MonoBehaviour
    {
        public enum HealthTarget
        {
            self,
            collider,
            other
        }

        public HealthTarget target;

        public float baseValue;
        public float distanceValue;
        public AnimationCurve distanceEffect;


        void OnCollisionEnter( Collision collisionInfo )
        {

        }

        void OnTriggerEnter( Collider collider )
        {
        }


        void DoDamageCalc( Component other )
        {
            Health health;

            if( target == HealthTarget.self )
            {
                health = GetComponent<Health>();
            }
            else
            {
                health = other.GetComponent<Health>();
            }

            float dist = (transform.position - other.transform.position).magnitude;
            distanceEffect.Evaluate( dist );
            health.current += baseValue;
        }
    }
}