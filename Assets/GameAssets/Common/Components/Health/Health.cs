﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                          Health.cs                          </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    15-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A generic health system.               </summary>
    /// <remarks> 
    ///     This class represents a basic health/damage system
    ///     that can be used in any game.
    ///                                                   </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    [AddComponentMenu( "Health/Health", 0 )]
    [RequireComponent(typeof(NetworkIdentity))]
    public class Health : NetworkBehaviour
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, SyncVar( hook ="OnCurrentHealth" ), Tooltip
        (   "The current health value."                               )]
        //--------------------------------------
        private float _current = 100;



        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The maximal value this health can reach."                )]
        //--------------------------------------
        private float _max      = 100;
        #endregion
        //......................................        



        //..............................................................
        #region            //  PUBLIC EVENTS  //        
        //--------------------------------------------------------------
        /// <summary>  Heath event handler delegate.           <summary>
        //--------------------------------------
        public delegate void EventHealthDelegate( NetworkIdentity senderId,
                                                  float current, 
                                                  float prev );


        //--------------------------------------------------------------
        /// <summary>  Heath event. Fired when the state of the 
        ///            health component changes.               <summary>
        //--------------------------------------
        public event EventHealthDelegate EventHealth;
        #endregion
        //......................................



        //..............................................................
        #region            //  PUBLIC PROPERTIES  //
        //--------------------------------------------------------------
        /// <summary> Gets and sets the current health.       </summary>
        /// <value> the new health value.                       </value>
        /// <remarks>
        ///     The new value cannot go bellow 0 or above <c>max</c>.
        ///     When the value of <c>current</c> changes,
        ///     <c>HealthEvent</c> will be raised.
        ///                                                   </remarks>
        //--------------------------------------
        public float current
        {
            get { return _current; }
            set { Set( value ); }
        }



        //--------------------------------------------------------------
        /// <summary> Gets and sets the max health.           </summary>
        /// <value> the new health cap                          </value>
        /// <remarks>
        ///     Setting <c>max</c> to a value below <c>current</c> will
        ///     result in also setting <c>current</c> to the same value.
        ///     This in turn will raise <c>HealthEvent</c>.
        ///                                                   </remarks>
        //--------------------------------------
        public float max
        {
            get { return _max; }
            set
            {
                _max = value;

                if( current > _max )
                    current = _max;
            }
        }
        #endregion
        //......................................



        //..............................................................
        #region               //  METHODS  //        
        //--------------------------------------------------------------
        /// <summary>  Heals by a given <c>amount</c>.        </summary>
        //--------------------------------------
        [Server]
        public void Heal( float amount )
        {
            Set( current + amount );
        }



        //--------------------------------------------------------------
        /// <summary>  Damages by a given <c>amount</c>.      </summary>
        //--------------------------------------
        [Server]
        public void Damage( float amount )
        {
            Set( current - amount );
        }



        //--------------------------------------------------------------
        /// <summary> Sets the health to the specified value. </summary>
        /// <remarks>
        ///     Will fire <c>EventHealth</c> if the new health value is
        ///     different from the current.                   </remarks>
        //--------------------------------------
        [Server]
        public void Set( float newHealth )
        {
            float oldHealth = current;
            newHealth = ClampHealth( newHealth );

            if( newHealth != oldHealth )
            {
                _current = newHealth;

                OnHealthChanged( oldHealth );
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Called by Unity at initialisation.     </summary>
        //--------------------------------------
        protected void Awake()
        {
            var listeners = GetComponents<IHealthListener>();

            foreach( var listener in listeners )
            {
                EventHealth += listener.OnHealthChanged;
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Fires the <c>EventHealth</c>.          </summary>
        //--------------------------------------
        private void OnHealthChanged( float oldValue )
        {           
            if( EventHealth != null )
            {
                //This is too noisy. Make it send events less often.
                //In fact it doesn't need to be a syncevent, we have a var already.

                EventHealth( GetComponent<NetworkIdentity>(), 
                             current, 
                             oldValue );
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Fires the <c>EventHealth</c>.          </summary>
        //--------------------------------------
        private void OnCurrentHealth( float newValue )
        {
            if( isServer )
                return;

            float oldValue = _current;
            _current = newValue;

            OnHealthChanged( oldValue );
        }


        //--------------------------------------------------------------
        /// <summary>  An utility to clmap health.            </summary>
        //--------------------------------------
        private float ClampHealth( float health )
        {
            return Mathf.Clamp( health, 0, max );
        }
        #endregion
        //......................................
    }
}