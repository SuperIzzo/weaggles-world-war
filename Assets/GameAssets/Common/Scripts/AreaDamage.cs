﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class AreaDamage : MonoBehaviour
    {
        public float damage = 25;

        void OnTriggerEnter( Collider col )
        {            
            if( NetworkServer.active )
            {
                Damage( col.gameObject, damage );
            }
        }

        void Damage( GameObject obj, float dmg )
        {
            // Damage all damagable components on the collider
            Health health = obj.GetComponent<Health>();

            if( health )
            {
                health.current -= dmg;
            }
        }
    }
}