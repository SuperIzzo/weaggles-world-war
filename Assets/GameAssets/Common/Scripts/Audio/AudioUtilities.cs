﻿using UnityEngine;
using System.Collections.Generic;

public class AudioUtilities : MonoBehaviour
{
    static AudioUtilities _instance;

    [SerializeField]
    AudioSource _genericSFX = null;

    static List<AudioSource> _createdSFX;



    // Use this for initialization
    protected void Awake ()
    {
        _instance = this;
	}



    protected void Start()
    {
        _createdSFX = new List<AudioSource>();
    }



    public static void PlaySFXAtPoint(  AudioClip clip, 
                                        Vector3 position, 
                                        float volume = 1,
                                        int priority = 128 )
    {
        AudioSource audioSource = GetFreeAudioSource();

        if( audioSource )
        {
            audioSource.transform.position = position;
            audioSource.clip = clip;
            audioSource.volume = volume;
            audioSource.priority = priority;
            audioSource.Play();
        }
    }



    private static AudioSource GetFreeAudioSource()
    {
        if( _createdSFX != null )
        {
            foreach( AudioSource audioSource in _createdSFX )
            {
                if( audioSource!=null && !audioSource.isPlaying )
                {
                    return audioSource;
                }
            }

            if( _instance )
            {
                AudioSource newAudioSource = Instantiate(_instance._genericSFX);
                newAudioSource.transform.parent = _instance.transform;
                _createdSFX.Add( newAudioSource );

                return newAudioSource;
            }            
        }

        return null;
    }
}
