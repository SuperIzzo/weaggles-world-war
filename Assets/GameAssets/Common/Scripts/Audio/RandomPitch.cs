﻿using UnityEngine;
using System.Collections;

public class RandomPitch : MonoBehaviour
{
    [SerializeField]
    float _minPitch = 0;

    [SerializeField]
    float _maxPitch = 0;

    AudioSource _audio;

	// Use this for initialization
	void Awake ()
    {
        _audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        _audio.pitch = Random.Range( _minPitch, _maxPitch );
	}
}
