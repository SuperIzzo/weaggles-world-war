﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                     DelayedAudioLoop.cs                     </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    11-Oct-2015                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> Loops an audio source at random intervals.</summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    [RequireComponent(typeof( AudioSource ))]    
    public class DelayedAudioLoop : MonoBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The minimal belay before the audio replays."             )]
        //--------------------------------------
        float minDelayTime = 0;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The maximal belay before the audio replays."             )]
        //--------------------------------------
        float maxDelayTime = 0;



        //--------------------------------------------------------------
        /// <summary>  A reference to the AudioSource component. </summary>
        //--------------------------------------
        AudioSource _audio;



        //--------------------------------------------------------------
        /// <summary>  Internal dalay timer.                  </summary>
        //--------------------------------------
        float _delayTimer;



        //--------------------------------------------------------------
        /// <summary>  Sets up the component.                 </summary>
        //--------------------------------------
        protected void Awake()
        {
            _audio = GetComponent<AudioSource>();
            _delayTimer = Random.Range( minDelayTime, maxDelayTime );
        }



        //--------------------------------------------------------------
        /// <summary>  Resets the audio loop.                 </summary>
        //--------------------------------------
        protected void Update()
        {
            if( !_audio.isPlaying )
            {
                _delayTimer -= Time.deltaTime;

                if( _delayTimer <= 0 )
                {
                    _audio.Play();
                    _delayTimer = Random.Range( minDelayTime, maxDelayTime );
                }
            }
        }
    }
}