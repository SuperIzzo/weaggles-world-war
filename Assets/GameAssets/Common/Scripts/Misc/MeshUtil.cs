﻿using UnityEngine;

public static class MeshUtil
{    
    //--------------------------------------------------------------
    /// <summary> Generates a vertex colored mesh copy.   </summary>
    /// <param name="mesh"> The mesh to be copied.          </param>
    /// <param name="col">  The colors of the vertices.     </param>
    /// <param name="suffix"> Suffix for the new mesh name. </param>
    //--------------------------------------
    public static Mesh ColoredMesh( Mesh mesh, Color32 col, string suffix = "" )
    {
        Mesh newMesh = new Mesh();

        newMesh.vertices = mesh.vertices;
        newMesh.triangles = mesh.triangles;
        newMesh.normals = mesh.normals;
        newMesh.uv = mesh.uv;
        newMesh.name = mesh.name + suffix;

        Color32[] colors = new Color32[mesh.vertexCount];

        for( int i = 0; i < mesh.vertexCount; i++ )
            colors[i] = col;

        newMesh.colors32 = colors;

        return newMesh;
    }
}
