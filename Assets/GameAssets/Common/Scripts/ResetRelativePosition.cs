﻿using UnityEngine;
using UnityEngine.Networking;


public class ResetRelativePosition : NetworkBehaviour
{
    [SerializeField]
    Vector3 _localPosition = Vector3.zero;

	protected void Update()
    {
        if( transform.parent )
        {
            transform.parent.position += transform.localPosition - _localPosition;
            transform.localPosition = _localPosition;
        }
    }
}
