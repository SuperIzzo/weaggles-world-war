﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class Drowning : NetworkBehaviour
    {
        [SerializeField]
        GameObject _bubbles = null;

        [SerializeField]
        float _damageOverTime = 1;

        [SerializeField]
        float _damageTick     = 0.5f;


        float       _damageTime = 0;
        Health      _health;
        GameObject  _bubblesInstance;



        protected void Awake()
        {
            _health = GetComponent<Health>();            
        }



        [ServerCallback]
        protected void OnTriggerStay( Collider col )
        {
            if( col.CompareTag( Tags.Water ) )
            {                
                if( _health )
                {
                    _damageTime += Time.fixedDeltaTime;
                    if( _damageTime >= _damageTick )
                    {
                        _health.Damage( _damageOverTime * _damageTime );
                        _damageTime = 0;
                    }
                }
            }
        }



        [ClientCallback]
        protected void OnTriggerEnter( Collider col )
        {
            if( col.CompareTag( Tags.Water ) )
            {
                if( !_bubblesInstance )
                {
                    _bubblesInstance = Instantiate( _bubbles );
                    _bubblesInstance.transform.SetParent( transform, false );
                }

                if( _bubblesInstance  )
                    _bubblesInstance.SetActive( true );
            }
        }



        protected void OnTriggerExit( Collider col )
        {
            if( col.CompareTag( Tags.Water ) )
            {
                if( _bubblesInstance )
                    _bubblesInstance.SetActive( false );
            }
        }
    }
}