﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;


    public class HurtSfx : MonoBehaviour, IHealthListener
    {        
        public AudioClip sound;
        public AudioClip overDamageSound;

        private float _soundCooldown;
        private const float _soundCooldownTime = 0.5f;



        public void OnHealthChanged( object sender, float current, float prev )
        {
            // Only works for default damage type for now
            if( enabled && 
                current<prev &&
                _soundCooldown <=0 )
            {
                if( current>0 )
                {
                    if( sound && (current-prev) >= 0.5 )
                        AudioUtilities.PlaySFXAtPoint( sound, transform.position );
                }
                else
                {
                    if( overDamageSound )
                        AudioUtilities.PlaySFXAtPoint( overDamageSound, transform.position );
                }

                _soundCooldown = _soundCooldownTime;
            }
        }



        protected void Update()
        {
            _soundCooldown -= Time.deltaTime;
        }
    }
}