﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;


    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> Damage animation trigger. </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class DamageAnim : NetworkBehaviour, IHealthListener
    {
        //--------------------------------------------------------------
        /// <summary> Animator reference. </summary>
        //--------------------------------------
        public Animator animator;



        //..............................................................
        #region              //  METHODS  //
        //--------------------------------------------------------------
        /// <summary> Setup references this instance. </summary>
        //--------------------------------------
        protected void Awake()
        {
            if( !animator )
            {
                animator = GetComponent<Animator>();
            }
        }



        //--------------------------------------------------------------
        /// <summary> Triggers the damage animation. </summary>
        //-------------------------------------- 
        public void OnHealthChanged( object sender, float current, float prev)
        {
            if( current<prev )
            {
                animator.SetTrigger( "TakingDamage" );
            }
        }
        #endregion
        //......................................
    }
}