﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;


    /// <summary>
    /// Component that changes a renderer's material based on remaining health.
    /// </summary>
    [RequireComponent(typeof(Health))]
    public class HealthMaterial : MonoBehaviour
    {
        // Eternal References        
        public Renderer targetRenderer;

        // Configuration
        public float        stage = 25.0f;
        public Material[]   materials = new Material[0];


        private Health health;


        /// <summary>
        /// Set the initial material
        /// </summary>
        void Start()
        {
            health = GetComponent<Health>();

            if( health != null )
            {
                SetMaterial();
                health.EventHealth += OnHealthChanged;
            }
        }



        /// <summary>
        /// 	Update the material on damage
        /// </summary>
        /// <param name="damage">Damage (ignored).</param>
        public void OnHealthChanged( object health, float current, float prev )
        {
            SetMaterial();
        }



        /// <summary>
        /// Sets the correct material based on health material.
        /// </summary>
        private void SetMaterial()
        {
            if( health != null )
            {
                int stageIndex = (int)((health.current - 0.0001f)/stage);
                stageIndex = Mathf.Clamp( stageIndex, 0, materials.Length - 1 );

                targetRenderer.material = materials[stageIndex];
            }
        }
    }
}