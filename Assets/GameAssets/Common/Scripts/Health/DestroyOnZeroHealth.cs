﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                   DestroyOnZeroHealth.cs                    </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    06-Oct-2015                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;


    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  Destroys a target on health depletion. </summary>    
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    public class DestroyOnZeroHealth : NetworkBehaviour, IHealthListener
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The destroy target. Destroys self if none set."          )]
        //--------------------------------------
        private GameObject _target;



        //--------------------------------------------------------------
        /// <summary> Health cahnged callback. </summary>
        /// <remarks>
        ///     Destroys the target game object, if none is set
        ///     the it will destroy this game object.
        /// </remarks>
        //--------------------------------------
        public void OnHealthChanged( object sender, float current, float prev )
        {
            if( current<=0 && isServer )
            {
                if( _target == null )
                    _target = gameObject;

                Destroy( _target );
            }
        }
    }
}