﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using System.Collections;

    [RequireComponent( typeof( TextMesh ) )]
    public class HealthWidget : MonoBehaviour
    {
        public Health health;
        private TextMesh textMesh;
        private Animator animator;


        // Use this for initialization
        void Start()
        {
            textMesh = GetComponent<TextMesh>();
            animator = GetComponent<Animator>();

            if( !health )
            {
                Debug.LogError( "health component not specified" );
            }
            else
            {
                health.EventHealth += OnHealthEvent;

                if( textMesh )
                {
                    textMesh.text = health.current.ToString();
                }
            }
        }


        void OnHealthEvent( object sender, float current, float prev )
        {
            textMesh.text = health.current.ToString();

            if( animator )
            {
                animator.SetTrigger( "Changed" );
            }
        }
    }
}