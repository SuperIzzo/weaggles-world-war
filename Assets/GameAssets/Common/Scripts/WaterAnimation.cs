﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      WaterAnimation.cs                      </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov                                     * 
 *                                                                            *
 * Permission is hereby granted,  free of charge,  to any person  obtaining a *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction,  including without limitation *
 * the rights to use, copy, modify,  merge, publish,  distribute, sublicense, *
 * and/or  sell copies  of the Software,  and to permit persons  to whom  the *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED,  INCLUDING BUT NOT LIMITED TO THE WARRANTIES  OF MERCHANTABILITY, *
 * FITNESS  FOR A PARTICULAR PURPOSE  AND NONINFRINGEMENT.  IN NO EVENT SHALL *
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *
 * LIABILITY,  WHETHER IN AN ACTION OF CONTRACT,  TORT OR OTHERWISE,  ARISING *
 * FROM,  OUT OF  OR  IN CONNECTION  WITH  THE SOFTWARE  OR THE USE  OR OTHER *
 * DEALINGS IN THE SOFTWARE.                                                  * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    29-Sep-2015                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
using UnityEngine;



//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
/// <summary>  A simple water animation         </summary>
/// <remarks> 
///     This effect is meant to be used with the standard
///     shaders in Unity 5. To setup up your material properly
///     assign normal maps to both the main and the
///     secondary map slots.
/// </remarks>
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
[RequireComponent(typeof(Renderer))]
public class WaterAnimation : MonoBehaviour
{
    //..............................................................
    #region                //  CONSTANTS  //
    //-------------------------------------------------------------- 
    const string _texture1 = "_MainTex";
    const string _texture2 = "_DetailAlbedoMap";
    #endregion
    //......................................



    //..............................................................
    #region           //  INSPECTOR SETTINGS  //
    //--------------------------------------------------------------
    [SerializeField, Tooltip
    (   "Speed of the main texture.\n" +
        "X,Y - constant speed     Z,W - random speed"             )]
    //--------------------------------------
    Vector4 _waterSpeed1 = Vector4.zero;


    //--------------------------------------------------------------
    [SerializeField, Tooltip
    (   "Speed of the secondary texture.\n" +
        "X,Y - constant speed     Z,W - random speed"             )]
    //--------------------------------------
    Vector4 _waterSpeed2 = Vector4.zero;
    #endregion
    //......................................



    //..............................................................
    #region           //  PRIVATE FIELDS  //
    //--------------------------------------------------------------
    Material _material;
    Vector2 _offset1;
    Vector2 _offset2;
    #endregion
    //......................................



    //..............................................................
    #region               //  METHODS  //
    //--------------------------------------------------------------
    /// <summary> Callback. Sets up internal references. </summary>
    //--------------------------------------
    protected void Awake ()
    {
        var meshRenderer = GetComponent<Renderer>();
        _material = meshRenderer.sharedMaterial;        
	}



    //--------------------------------------------------------------
    /// <summary> Callback. Animates texture offsets. </summary>
    //--------------------------------------
    protected void Update ()
    {
        _offset1.x += _waterSpeed1.x * Time.deltaTime;
        _offset1.x += (Random.value-0.5f) * 2 * _waterSpeed1.z * Time.deltaTime;
        _offset1.y += _waterSpeed1.y * Time.deltaTime;
        _offset1.y += (Random.value - 0.5f) * 2 * _waterSpeed1.w * Time.deltaTime;

        _offset2.x += _waterSpeed2.x * Time.deltaTime;
        _offset2.x += (Random.value - 0.5f) * 2 * _waterSpeed2.z * Time.deltaTime;
        _offset2.y += _waterSpeed2.y * Time.deltaTime;
        _offset2.y += (Random.value - 0.5f) * 2 * _waterSpeed2.w * Time.deltaTime;

        _material.SetTextureOffset( _texture1, _offset1 );
        _material.SetTextureOffset( _texture2, _offset2 );
    }
    #endregion
    //......................................
}
