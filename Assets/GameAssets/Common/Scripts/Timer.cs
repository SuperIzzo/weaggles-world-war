/// <summary>
/// A countdown timer.
/// </summary>
[System.Serializable()]
public class Timer
{
	// Configuration
	public float resetTime;

	// The current time
	public float currentTime { get{ return _countDown; } }

	// State
	private float _countDown;

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Timer"/> is running.
	/// </summary>
	/// <value><c>true</c> if running; otherwise, <c>false</c>.</value>
	public bool isRunning
	{
		get{ return _countDown>0; }
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Timer"/> class.
	/// </summary>
	/// <param name="time">The default reset time.</param>
	public Timer( float time =0 )
	{
		resetTime = time;
        _countDown = 0;
    }

	/// <summary>
	/// Step the specified deltaTime.
	/// </summary>
	/// <param name="deltaTime">Delta time.</param>
	public void Step( float deltaTime )
	{
		if( _countDown>0 )
		{
			_countDown -= deltaTime;
		}
	}

	/// <summary>
	/// Stop the timer.
	/// </summary>
	public void Stop()
	{
		_countDown = 0;
	}

	/// <summary>
	/// Reset the timer (with an optional time).
	/// </summary>
	/// <param name="time">Time.</param>
	public void Reset( float time = -1 )
	{
		if( time>0 )
		{
			_countDown = time;
		}
		else
		{
			_countDown = resetTime;
		}
	}
}

