﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;


    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  Replaces a target on health depletion. </summary>    
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    public class ReplaceOnZeroHealth : NetworkBehaviour, IHealthListener
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The destroy target. Destroys self if none set."          )]
        //--------------------------------------
        private GameObject _target;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The replacement prefab. Will instantiate a new object."  )]
        //--------------------------------------
        private GameObject _replacement = null;



        //--------------------------------------------------------------
        /// <summary> Health cahnged callback. </summary>
        /// <remarks>
        ///     Destroys the target game object, if none is set
        ///     the it will destroy this game object. Then it
        ///     instantiates a replacement object at the target's
        ///     location and orientation.
        /// </remarks>
        //--------------------------------------
        public void OnHealthChanged( object sender, float current, float prev )
        {
            if( current<=0 && hasAuthority )
            {
                // Set target to self if not explicitly set
                if( !_target )
                {
                    _target = gameObject;
                }

                // Crate the replacement object first              
                if( _replacement )
                {
                    var replacement =
                        Instantiate(_replacement, 
                                    _target.transform.position, 
                                    _target.transform.rotation ) as GameObject;

                    NetworkServer.Spawn( replacement );
                }                

                // Destroy the old object
                Destroy( _target );
            }
        }
    }
}
