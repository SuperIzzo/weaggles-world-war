﻿using System.Collections.Generic;
using UnityEngine;

public class SingletonGameObject : MonoBehaviour
{    
    static Dictionary<string, GameObject> _singletons;



    //--------------------------------------------------------------
    /// <summary>  Retrieves a singleton by object name.  </summary>
    //--------------------------------------
    private static GameObject GetSingleton( string name )
    {
        if( _singletons!=null )
        {
            if( _singletons.ContainsKey( name ) )
                return _singletons[name];
        }

        return null;
    }



    //--------------------------------------------------------------
    /// <summary>  Registers an object as a singleton.    </summary>
    //--------------------------------------
    private static void SetSingleton( string name, GameObject obj )
    {
        if( _singletons == null )
        {
            _singletons = new Dictionary<string, GameObject>();
        }

        _singletons[name] = obj;        
    }



    //--------------------------------------------------------------
    /// <summary>  Unity callback; destroys us if we duplicate.  </summary>
    //--------------------------------------
    void Awake ()
    {
        GameObject existingSingleton = GetSingleton( gameObject.name );

        if( existingSingleton && existingSingleton.activeInHierarchy )
        {
            gameObject.SetActive( false );
            Destroy( gameObject );
        }
        else
        {
            DontDestroyOnLoad( gameObject );
            SetSingleton( gameObject.name, gameObject );
        }
	}
}
