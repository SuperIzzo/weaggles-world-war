﻿using UnityEngine;
using System.Collections;

public class DelayedDestruction : MonoBehaviour
{
	public float destructionTime;

	// Use this for initialization
	void Start () {
		Destroy( gameObject, destructionTime );
	}

}
