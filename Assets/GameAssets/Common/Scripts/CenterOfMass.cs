﻿using UnityEngine;
using System.Collections;

[AddComponentMenu( "Physics/Center Of Mass")]
[RequireComponent( typeof(Rigidbody) )]
public class CenterOfMass : MonoBehaviour
{
	public Vector3 centerOfMass;

	// Use this for initialization
	void Start ()
	{
		GetComponent<Rigidbody>().centerOfMass = centerOfMass;
	}

#if UNITY_EDITOR
    protected void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;        
        Gizmos.DrawSphere( transform.TransformPoint(centerOfMass), 0.02f );
    }
#endif
}
