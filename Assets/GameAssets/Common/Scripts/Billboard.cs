﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Billboard : MonoBehaviour
{
	public Transform _target;
	public bool faceAway = false;

	void Awake()
	{
		if( _target==null )
		{
			_target = Camera.main.transform;
		}
	}
	
	void Update()
	{
		// Skip this if we don't have a camera to look at
		if( _target == null )
			return;

		Vector3 orientation;
		if( faceAway )
		{
			orientation = Vector3.back;
		}
		else
		{
			orientation = Vector3.forward;
		}

		transform.LookAt(transform.position + _target.rotation * orientation,
		                 _target.rotation * Vector3.up);
	}
}