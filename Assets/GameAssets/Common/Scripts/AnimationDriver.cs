﻿using UnityEngine;
using System.Collections;


[System.Flags]
enum UpdateFrequency
{
    Update = (1 << 0),
    FixedUpdate = (1 << 1),
    LateUpdate = (1 << 2),
    Editor = (1 << 3),
}


[ExecuteInEditMode]
public class AnimationDriver : MonoBehaviour
{
    [SerializeField][EnumFlagsAttribute]
    UpdateFrequency _animateOn = UpdateFrequency.Update | UpdateFrequency.Editor;

    protected void Update()
    {
#if UNITY_EDITOR
        if( (_animateOn & UpdateFrequency.Update) > 0
            || ((_animateOn & UpdateFrequency.Editor) > 0 && !Application.isPlaying) )
        {
            Animate();
        }
#else
        if( (_animateOn & UpdateFrequency.Update) > 0 )
        {
            Animate();
        }
#endif

    }

    protected void FixedUpdate()
    {
        if( (_animateOn & UpdateFrequency.FixedUpdate) > 0 )
        {
            Animate();
        }
    }

    protected void LateUpdate()
    {
        if( (_animateOn & UpdateFrequency.LateUpdate) > 0 )
        {
            Animate();
        }
    }

    protected virtual void Animate()
    {
    }
}