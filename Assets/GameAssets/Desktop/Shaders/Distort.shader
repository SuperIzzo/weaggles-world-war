﻿Shader "Custom/Distort" 
{
    Properties
    {
        _DistortionMap("Distortion Map", 2D) = "bump" {}
        _DistortionStrenght("Distortion Map", Float) = 0.01
    }
    
    SubShader
    {
        Tags{ "Queue" = "Overlay" }
        LOD 400

        ZWrite Off
        ZTest  Off

        GrabPass{ "_DistortionGrab" }

        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 uvGrab : TEXCOORD1;
            };


            sampler2D _DistortionMap;
            sampler2D _DistortionGrab;
            float     _DistortionStrenght;

            v2f vert(appdata_base v)
            {
                v2f output;
                output.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                output.uv = float2(v.texcoord.xy);
                output.uvGrab = ComputeGrabScreenPos( output.pos );

                return output;
            }


            

            float4 frag(v2f IN) : COLOR
            {
                float2 uv = IN.uv.xy; // IN.grabUV.w;
                float2 uvGrab = IN.uvGrab.xy / IN.uvGrab.w;
                float3 disp = UnpackNormal( tex2D(_DistortionMap, uv) );

                return tex2D(_DistortionGrab, uvGrab + disp.xy * _DistortionStrenght);
            }

            ENDCG
        }
    }
}