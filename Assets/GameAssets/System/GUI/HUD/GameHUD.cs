﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;


    public class GameHUD : MonoBehaviour
    {
        public enum ItemSlotPosition
        {
            None,
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight
        }

        public ItemSlotHUD topLeftItemSlot;
        public ItemSlotHUD topRightItemSlot;
        public ItemSlotHUD bottomLeftItemSlot;
        public ItemSlotHUD bottomRightItemSlot;

        public ItemSlotHUD GetItemSlot( ItemSlotPosition pos )
        {
            switch( pos )
            {
                case ItemSlotPosition.TopLeft:
                    return topLeftItemSlot;
                case ItemSlotPosition.TopRight:
                    return topRightItemSlot;
                case ItemSlotPosition.BottomLeft:
                    return bottomLeftItemSlot;
                case ItemSlotPosition.BottomRight:
                    return bottomRightItemSlot;
            }

            return null;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}