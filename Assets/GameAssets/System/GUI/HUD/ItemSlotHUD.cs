﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.UI;


    public class ItemSlotHUD : MonoBehaviour
    {
        public Image        itemIconUI;
        public Text         itemCountUI;
        ItemSlot            _slot;
        Animator            animator;


        // Use this for initialization
        void Start()
        {
            if( !itemIconUI )
            {
                itemIconUI = GetComponent<Image>();
            }

            animator = GetComponent<Animator>();
        }

        public ItemSlot itemSlot
        {
            get { return _slot; }
            set { _slot = value; }
        }

        // Update is called once per frame
        void Update()
        {
            if( _slot )
            {
                Item itemType = _slot.GetItem();
                int itemCount = _slot.GetItemCount();

                if( itemIconUI && itemType )
                {
                    itemIconUI.sprite = itemType.itemIcon;
                }

                if( itemCountUI )
                {
                    itemCountUI.text = "x" + itemCount;
                }

                if( itemCount > 0 )
                {
                    animator.SetBool( "visible", true );
                }
                else
                {
                    animator.SetBool( "visible", false );
                }
            }
            else
            {
                animator.SetBool( "visible", false );
            }
        }
    }
}