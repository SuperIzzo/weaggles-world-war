﻿using UnityEngine;

public class Dialog : MonoBehaviour
{
	public Animator animator;

	// Use this for initialization
	void Start ()
	{
		if( animator == null )
			animator = GetComponent<Animator>();
	}

	public void OnShow()
	{
		if( animator )
		{
			gameObject.SetActive(true);
			animator.SetBool( "Visible", true );
		}
		else
		{
			gameObject.SetActive(true);
		}
	}

	
	public void OnHide()
	{
		if( animator )
		{
			animator.SetBool( "Visible", false );
		}
		else
		{
			gameObject.SetActive(false);
		}
	}

	public void OnShowAnimationEnd()
	{
	}

	public void OnHideAnimationEnd()
	{
		
		gameObject.SetActive(false);
	}
}
