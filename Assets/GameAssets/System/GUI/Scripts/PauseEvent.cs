﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                        PauseEvent.cs                        </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2014-2016  Hristoz Stefanov - All Rights Reserved          * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    03-Feb-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;

    public class PauseEvent : MonoBehaviour
    {
        public string menuButton = Controls.menu;
        public string pauseDialog = "Pause";

        void Update()
        {
            if( InputManager.GetButtonDown( menuButton ) )
            {
                GameGUI gui = GameGUI.instance;

                if( gui )
                {
                    if( gui.currentDialog )
                    {
                        gui.HideDialog( pauseDialog );
                    }
                    else
                    {
                        gui.ShowDialog( pauseDialog );
                    }
                }
            }
        }
    }
}