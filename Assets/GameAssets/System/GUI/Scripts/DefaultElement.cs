﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DefaultElement : MonoBehaviour
{
	public GameObject	defaultSelected;	
	private bool 		activated = false;


	void Update()
	{
		if( !activated )
		{
			activated = true;
			OnActivate();
		}
	}
	
	void OnActivate()
	{	
		EventSystem eventSystem = EventSystem.current;
		if( eventSystem!=null )
			eventSystem.SetSelectedGameObject( defaultSelected );
	}
	
	void OnDisable()
	{
		activated = false;
	}
}
