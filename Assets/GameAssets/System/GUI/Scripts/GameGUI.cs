﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;


    public class GameGUI : MonoBehaviour
    {
        [Serializable]
        public struct DialogBoxNamePair
        {
            public string name;
            public GameObject dialog;
        }

        private static GameGUI m_instance;
        public static GameGUI instance
        {
            get
            {
                if( m_instance == null )
                {
                    m_instance = GameObject.FindObjectOfType<GameGUI>();
                }

                return m_instance;
            }
        }

        private GameObject m_currentDialog;
        public GameObject currentDialog
        {
            get
            {
                return m_currentDialog;
            }

            set
            {
                DoHideDialog( m_currentDialog );
                m_currentDialog = value;
                DoShowDialog( m_currentDialog );
            }
        }

        [SerializeField]
        private List<DialogBoxNamePair> m_dialogBoxes = null;
        public GameHUD hud;

        public void ShowDialog( GameObject obj )
        {
            currentDialog = obj;
        }

        public void ShowDialog( string name )
        {
            GameObject dialog = GetNamedDialog( name );

            if( dialog )
                currentDialog = dialog;
        }

        public void HideDialog()
        {
            currentDialog = null;
        }

        public void HideDialog( GameObject obj )
        {
            if( currentDialog == obj )
                currentDialog = null;
        }

        public void HideDialog( string name )
        {
            GameObject dialog = GetNamedDialog( name );

            if( dialog )
                HideDialog( dialog );
        }

        private GameObject GetNamedDialog( string name )
        {
            foreach( DialogBoxNamePair dialog in m_dialogBoxes )
            {
                if( dialog.name.Equals( name ) )
                {
                    return dialog.dialog;
                }
            }

            return null;
        }

        private void DoHideDialog( GameObject dialog )
        {
            if( dialog )
            {
                Dialog dialogComponent = dialog.GetComponent<Dialog>();

                if( dialogComponent )
                    dialogComponent.OnHide();
                else
                    dialog.SetActive( false );
            }
        }

        private void DoShowDialog( GameObject dialog )
        {
            if( dialog )
            {
                Dialog dialogComponent = dialog.GetComponent<Dialog>();

                if( dialogComponent )
                    dialogComponent.OnShow();
                else
                    dialog.SetActive( true );
            }
        }
    }
}