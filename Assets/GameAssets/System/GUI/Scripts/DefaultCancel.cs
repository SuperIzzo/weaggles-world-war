﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.EventSystems;


    public class DefaultCancel : MonoBehaviour
    {
        public GameObject cancel;

        public void Update()
        {
            if( InputManager.GetButtonDown( Controls.decline ) )
            {
                if( cancel )
                {
                    ISubmitHandler submitter = (ISubmitHandler) cancel.GetComponent(typeof(ISubmitHandler));
                    if( submitter != null )
                        submitter.OnSubmit( new BaseEventData( EventSystem.current ) );
                }
            }
        }
    }
}