﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.SceneManagement;

    public class GUIActions : MonoBehaviour
    {
        public void ChangeScene( string sceneName )
        {
            var netMan = NetworkManager.singleton;

            if( netMan )
            {
                netMan.ServerChangeScene( sceneName );
            }
            else
            {
                SceneManager.LoadScene( sceneName );
            }
        }



        public void ResetScene()
        {
            var netMan = NetworkManager.singleton;

            if( netMan )
            {
                netMan.ServerChangeScene( SceneManager.GetActiveScene().name );
            }
            else
            {
                SceneManager.LoadScene( SceneManager.GetActiveScene().name );
            }
        }



        public void NextScene()
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex + 1 );
        }



        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
		//Application.OpenURL(webplayerQuitURL);
#else
		Application.Quit();
#endif
        }



        public void ShowDialog( string name )
        {
            GameGUI gui = GameGUI.instance;

            if( gui )
            {
                gui.ShowDialog( name );
            }
        }



        public void HideDialog()
        {
            GameGUI gui = GameGUI.instance;

            if( gui )
            {
                gui.currentDialog = null;
            }
        }



        public void ToggleHosting()
        {
            NetworkServer.dontListen = !NetworkServer.dontListen;
        }
    }
}