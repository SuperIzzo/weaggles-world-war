﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class TextShadow : MonoBehaviour
{
	public Text text;
	private Text myText; 

	// Use this for initialization
	void Start ()
	{
		myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		myText.text = text.text;
	}
}
