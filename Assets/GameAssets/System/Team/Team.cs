﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                           Team.cs                           </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    17-Sep-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> Team identity component. </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class Team : NetworkBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "Unique team identifier."                                 )]
        //--------------------------------------
        TeamID _team;



        //--------------------------------------------------------------
        /// <summary> Gets or sets the team id.               </summary> 
        //--------------------------------------
        public TeamID teamID
        {
            get { return _team; }
            set { _team = value; }
        }



        //--------------------------------------------------------------
        /// <summary> Gets or sets the team color.            </summary>
        //--------------------------------------
        public Color color
        {
            get
            {
                return TeamManager.GetTeamColor( teamID );
            }
        }



        //--------------------------------------------------------------
        /// <summary> Changes this team to another. </summary> 
        /// <param name="other"> the other team </param>
        //--------------------------------------
        public void Assign( Team other )
        {
            teamID = other.teamID;
        }



        //--------------------------------------------------------------
        /// <summary>  Returns whether this <see cref="Team"/> 
        ///            and another are allies.                </summary>
        /// <remarks>
        ///     The function returns false if the team other team is 
        ///     null instead of raising an error. This is intended. 
        ///                                                   </remarks>
        /// <returns> 
        ///     <c>true</c> if this and the other team are allies; 
        ///     otherwise, <c>false</c>.
        ///                                                   </returns>
        /// <param name="other"> the other team                 </param>
        //--------------------------------------
        public bool IsAlly( Team other )
        {
            if( other != null )
                return TeamManager.IsAlly( _team, other._team );
            else
                return false;
        }



        //--------------------------------------------------------------
        /// <summary>  Returns whether this <see cref="Team"/> 
        ///            and another are allies.                </summary>
        /// <returns> 
        ///     <c>true</c> if this and the other team are allies; 
        ///     otherwise, <c>false</c>.
        ///                                                   </returns>
        /// <param name="teamID"> the other team id             </param>
        //--------------------------------------
        public bool IsAlly( TeamID teamID )
        {
            return TeamManager.IsAlly( _team, teamID );
        }



        //--------------------------------------------------------------
        /// <summary>  Returns whether this <see cref="Team"/> 
        ///            and another are allies.                </summary>
        /// <returns>
        ///     <c>true</c> if this and the other team are allies;
        ///     otherwise, <c>false</c>.                      </returns>
        /// <param name="other"> component from another object  </param>
        //--------------------------------------
        public bool IsAlly( Component other )
        {
            return IsAlly( other.GetComponent<Team>() );
        }



        //--------------------------------------------------------------
        /// <summary>  Returns whether this <see cref="Team"/> 
        ///            and another are allies.                </summary>
        /// <remarks>
        ///     The function returns false if the eather team is null
        ///     instead of raising an error. This is intended.
        ///                                                   </remarks>
        /// <returns>
        ///     <c>true</c> if this and the other team are allies;
        ///     otherwise, <c>false</c>.
        ///                                                   </returns>
        /// <param name="other"> the other team </param>
        //--------------------------------------
        public static bool IsAlly( Team team, Team otherTeam )
        {
            if( team != null )
                return team.IsAlly( otherTeam );
            else
                return false;
        }

        

        //--------------------------------------------------------------
        /// <summary>  Serves as a hash function 
        ///            for a <see cref="Team"/> object.       </summary>
        /// <returns> 
        ///     A hash code for this instance that is suitable 
        ///     for use in hashing algorithms and data structures 
        ///     such as a hash table.                         </returns>
        //--------------------------------------
        public override int GetHashCode()
        {
            return (int)_team;
        }        
    }
}