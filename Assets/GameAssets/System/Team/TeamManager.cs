﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TeamID
{
    None,
    Green,
    Red,
    Blue,
    Yellow,
    Purple
}

public class TeamManager : MonoBehaviour
{
    [SerializeField]
    TeamInfo[] _teams;

    Dictionary<TeamID, TeamInfo> _teamMap;

    static TeamManager _active;
    public static TeamManager active
    {
        get
        {
            if( !_active || !_active.enabled )
            {
                _active = FindObjectOfType<TeamManager>();
            }

            return _active;
        }

        set
        {
            _active = value;
        }
    }



	// Use this for initialization
	void Start ()
    {
        _teamMap = new Dictionary<TeamID, TeamInfo>( 
            Enum.GetNames(typeof(TeamID)).Length  );

	    foreach( TeamInfo team in _teams )
        {
            _teamMap[team.team] = team;
        }
	}



    public static Color GetTeamColor( TeamID teamID )
    {
        if( active )
        {
            if( active._teamMap[teamID] != null )
            {
                return active._teamMap[teamID].color;
            }
            else
            {
                Debug.LogError( "No configuration for team " + teamID );
                return Color.clear;
            }
        }
        else
        {
            Debug.LogError( "No active team manager in the scene. ");
            return Color.clear;
        }
    }



    public static bool IsAlly( TeamID teamA, TeamID teamB )
    {
        // For now keep things simple
        // Maybe later we may have alliances (maybe)
        return teamA == teamB;
    }


    [Serializable]
    class TeamInfo
    {
        public TeamID       team = TeamID.None;
        public Color        color = Color.clear;
    }
}
