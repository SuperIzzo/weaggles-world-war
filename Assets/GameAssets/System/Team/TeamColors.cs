﻿namespace Izzo.WeagglesWorldWar
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent( typeof(MeshFilter))]
    public class TeamColors : MonoBehaviour
    {
        static Dictionary< MeshTeamTuple, Mesh> _coloredMeshes;

        [SerializeField]
        Team        _team;

        Mesh        _originalMesh;
        TeamID      _meshTeam;



        // Use this for initialization
        void Start()
        {
            if( !_team )
            {
                _team = GetComponentInParent<Team>();
            }

            if( _team )
            {
                ResetMesh();
            }
        }



        void Update()
        {
            if( _team && _team.teamID != _meshTeam )
            {
                ResetMesh();
            }
        }



        private void ResetMesh()
        {
            if( _coloredMeshes == null )
            {
                _coloredMeshes = new Dictionary<MeshTeamTuple, Mesh>();
            }

            MeshFilter filter = GetComponent<MeshFilter>();

            if( !_originalMesh )
            {
                _originalMesh = filter.sharedMesh;
            }

            _meshTeam = _team.teamID;
            var colorMeshKey = new MeshTeamTuple( _originalMesh, _meshTeam);

            if( !_coloredMeshes.ContainsKey( colorMeshKey ) )
            {
                _coloredMeshes[colorMeshKey] =
                    MeshUtil.ColoredMesh( _originalMesh, _team.color,
                    " - Team " + _team.teamID.ToString() );
            }

            if( _coloredMeshes.ContainsKey( colorMeshKey ) )
            {
                filter.mesh = _coloredMeshes[colorMeshKey];
            }
        }


        struct MeshTeamTuple
        {
            internal readonly Mesh   mesh;
            internal readonly TeamID teamID;

            internal MeshTeamTuple( Mesh mesh, TeamID teamID )
            {
                this.mesh = mesh;
                this.teamID = teamID;
            }
        }
    }
}