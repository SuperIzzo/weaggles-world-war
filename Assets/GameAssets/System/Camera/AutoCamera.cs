﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                        AutoCamera.cs                        </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    29-Sep-2015                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>   Automatic camera position and orientation 
    ///             conrolled by aspect ratio.            </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    [ExecuteInEditMode, RequireComponent( typeof( Camera ) )]
    public class AutoCamera : MonoBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (  "Animation clip using aspect ratio as a control variable." )]
        //--------------------------------------
        AnimationClip _animation = null;



        //--------------------------------------------------------------
        /// <summary> A reference to the camera component.    </summary>
        //--------------------------------------
        Camera _camera;



        //--------------------------------------------------------------
        /// <summary> Initializes the component.              </summary>
        //--------------------------------------
        protected void Awake()
        {
            _camera = GetComponent<Camera>();

            if( !_animation )
                Debug.LogError( "'animation' not set." );
        }



        //--------------------------------------------------------------
        /// <summary> Updates the component.                  </summary>
        //--------------------------------------
        protected void Update()
        {
            if( _animation )
                _animation.SampleAnimation( gameObject, _camera.aspect );
        }
    }
}