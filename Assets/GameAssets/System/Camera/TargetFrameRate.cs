﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class TargetFrameRate : MonoBehaviour
{
    public int targetFrameRateMin = 60;
    public int targetFrameRateMax = 60;

    // Use this for initialization
    void Start()
    {
        QualitySettings.vSyncCount = 0;        
    }

    void Update()
    {
        Application.targetFrameRate = Random.Range( targetFrameRateMin, targetFrameRateMax );
        Time.timeScale = Random.Range( 1.0f, 2.3f );
        GetComponent<MotionBlur>().blurAmount = Random.value*0.3f+0.1f;
    }
}
