﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                       ItemManager.cs                        </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    21-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using System.Collections.Generic;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> An item database.                       </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class ItemManager : MonoBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (  "List of registered items."                                )]
        //--------------------------------------
        List<Item> _itemList = null;



        //--------------------------------------------------------------
        /// <summary> Item database, items are index by name. </summary>
        //--------------------------------------
        Dictionary<string,Item> _itemIDDictionary;



        //--------------------------------------------------------------
        /// <summary>  Sngleton instance cache.               </summary>
        //--------------------------------------
        private static ItemManager _instance;



        //--------------------------------------------------------------
        /// <summary>  Singleton instance.                    </summary>
        //--------------------------------------
        private static ItemManager instance
        {
            get
            {
                if(!_instance)
                {
                    _instance = FindObjectOfType<ItemManager>();
                }

                return _instance;
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Retrives an item by name.              </summary>
        /// <param name="itemName"> the name of the item.       </param>
        /// <returns>  the item with the given name; or 
        ///            null if non found.                     </returns>
        //--------------------------------------
        public static Item FindItem( string itemName )
        {
            if( instance )
            {
                return instance.FindItemImpl( itemName );
            }
            else
            {
                Debug.LogError( "No instance of ItemManager exists in the scene." );
                return null;
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Retrives an item by name.              </summary>
        /// <param name="itemName"> the name of the item.       </param>
        /// <returns>  the item with the given name; or 
        ///            null if non found.                     </returns>
        //--------------------------------------
        private Item FindItemImpl( string itemName )
        {
            Item result;

            if( _itemIDDictionary.TryGetValue( itemName, out result ) )
            {
                return result;
            }
            else
            {
                Debug.LogWarning( "Item with name '" + itemName + "' not found." );
                return null;
            }
        }



        //--------------------------------------------------------------
        /// <summary> Initializes the component.              </summary>
        //--------------------------------------
        protected void Start()
        {
            _itemIDDictionary = new Dictionary<string, Item>();

            foreach( Item item in _itemList )
            {
                _itemIDDictionary.Add( item.name, item );
            }
        }                
    }
}