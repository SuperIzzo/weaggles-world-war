﻿using UnityEngine;
using System.Collections;

public class TestOptions : MonoBehaviour
{
	Light mainLight;

	// Use this for initialization
	void Start ()
	{
		Light[] lights = FindObjectsOfType<Light>();

		foreach( Light aLight in lights )
		{
			if( aLight.type == LightType.Directional )
			{
				mainLight = aLight;
				break;
			}
		}
	}

	void OnGUI()
	{

		GUI.Label( new Rect( 0,0, 50, 20 ), "Volume" );
		AudioListener.volume = GUI.HorizontalSlider( 
			                            new Rect( 50,5, 100, 20 ), 
										AudioListener.volume, 0, 100 );

		if( mainLight )
		{
			bool shadowsEnabled = (mainLight.shadows != LightShadows.None);
			GUI.Label( new Rect( 0,20, 50, 20 ), "Shadows" );
			shadowsEnabled = GUI.Toggle( new Rect( 50,20, 100, 20 ), shadowsEnabled, "" );

			if( shadowsEnabled )
			{
				mainLight.shadows = LightShadows.Soft;
			}
			else
			{
				mainLight.shadows = LightShadows.None;
			}
		}
	}
}
