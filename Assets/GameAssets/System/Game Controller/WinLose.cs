﻿namespace Izzo.WeagglesWorldWar
{
    using System.Collections.Generic;
    using UnityEngine;


    // The win condition is:
    // * Destroy all enemy tanks UNTIL enemy resource is depleted
    //						OR
    // * Destroy all enemy tanks AND destroy all enemy spawners, so that
    // enemy resources cannot deplete
    // The lose condition is symetrical, except it refers to your resources,
    // units and spawner

    // It could be summarized as:
    //	If only one team of units remains:
    //		If no spawners of different team exist:
    //			the first team wins.
    //		If there are any spawners from a different team:
    // 			If they cannot produce a unit
    //				the first team wins
    //	Any other scenario is undecisive

    public class WinLose : MonoBehaviour
    {
        public Timer checkTimer = new Timer( 5.0f );

        private Team winningTeam;
        private GameGUI gui;

        void Awake()
        {
            gui = GameGUI.instance;
        }

        // Update is called once per frame
        void Update()
        {
            checkTimer.Step( Time.deltaTime );
            if( winningTeam == null && !checkTimer.isRunning )
            {
                winningTeam = GetWinningTeam();
                checkTimer.Reset();

                if( winningTeam != null )
                {
                    List<Player> localPlayers = Player.localPlayers;

                    // "If there are no players
                    //  then no one wins or loses"
                    //                  - Confucizzos
                    bool win =  (localPlayers.Count > 0);
                    bool lose = (localPlayers.Count > 0);

                    // Complete victory if all local players win
                    foreach( Player player in localPlayers )
                    {
                        win &= winningTeam.IsAlly( player.teamID );
                    }

                    // Complete loss if all local players lose
                    foreach( Player player in localPlayers )
                    {
                        lose &= !winningTeam.IsAlly( player.teamID );
                    }

                    if( win )
                    {
                        gui.ShowDialog( "Victory" );
                    }
                    else if( lose )
                    {
                        gui.ShowDialog( "Loss" );
                    }
                    else
                    {
                        throw new System.NotImplementedException( "Add a neutral eding dialog, please!" );
                    }
                }
            }
        }


        // Returns the last remaining team which still has units in play
        // If none or more than one team remain the function returns null; 
        private static Team GetLastUnitTeam()
        {
            Team onlyTeam = null;
            GameObject[] units = GameObject.FindGameObjectsWithTag( "Unit" );

            foreach( GameObject unit in units )
            {
                // We don't count destroyed objects
                if( unit == null )
                    continue;

                Team unitTeam = unit.GetComponent<Team>();

                // We don't count teamless units
                if( unitTeam == null )
                    continue;

                if( onlyTeam == null )
                {
                    onlyTeam = unitTeam;
                }
                else
                {
                    // If enemies, terminate here
                    if( !onlyTeam.IsAlly( unitTeam ) )
                        return null;
                }
            }

            return onlyTeam;
        }

        private static Team GetWinningTeam()
        {
            Team team = GetLastUnitTeam();

            if( team == null )
                return null;

            GameObject[] spawners = GameObject.FindGameObjectsWithTag( "Spawner" );

            foreach( GameObject spawner in spawners )
            {
                Team spawnerTeam = spawner.GetComponent<Team>();

                // Ignore allies and non-alligned spawners
                if( spawnerTeam == null || spawnerTeam.IsAlly( team ) )
                    continue;

                IUnitSpawner spawnerComponent = spawner.GetComponent<IUnitSpawner>();
                if( spawnerComponent != null && spawnerComponent.CanSpawn() )
                {
                    return null;
                }
            }

            return team;
        }
    }
}