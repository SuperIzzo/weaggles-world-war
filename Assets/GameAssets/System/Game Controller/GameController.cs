/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      GameController.cs                      </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    22-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> Loops an audio source at random intervals.</summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class GameController : MonoBehaviour
    {
        //--------------------------------------------------------------
        /// <summary>  Singleton instance cache. </summary>
        //--------------------------------------
        static GameController _instance;



        //--------------------------------------------------------------
        /// <summary>  Gets the singleton instance. </summary>
        //--------------------------------------
        public static GameController intance
        {
            get
            {
                if( _instance == null )
                {
                    _instance = FindObjectOfType<GameController>();
                }

                return _instance;
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Holds true if the game is pause; 
        ///            false otherwise.                       </summary>
        //--------------------------------------
        bool _isPaused = false;



        //--------------------------------------------------------------
        /// <summary>  Gets or sets the pause status of the game. </summary>
        //--------------------------------------
        public bool isPaused
        {
            get
            {
                return _isPaused;
            }

            set
            {
                if( value )
                    Pause();
                else
                    Resume();
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Sets up the singleton component.       </summary>
        //--------------------------------------
        protected void Awake()
        {
            if( !_instance )
            {
                _instance = this;
            }
            else if( _instance != this )
            {
                // Automatically disable duplicates                
                enabled = false;
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Pauses the game (using timeScale).     </summary>
        //--------------------------------------
        public void Pause()
        {
            _isPaused = true;
            Time.timeScale = 0;
        }



        //--------------------------------------------------------------
        /// <summary>  Resumes the game (using timeScale).    </summary>
        //--------------------------------------
        public void Resume()
        {
            _isPaused = false;
            Time.timeScale = 1;
        }
    }
}
