﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      IInputManager.cs                       </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2014-2016  Hristoz Stefanov - All Rights Reserved          * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    03-Feb-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> A general InputManager interface.       </summary>
    /// <remarks>
    ///     Can be implemented by various input generators,
    ///     decorators or filters to collect and manipulate 
    ///     data from input provides.                     </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public interface IInputManager
    {
        Vector2 mousePosition { get; }
        bool mousePresent { get; }
        bool touchSupported { get; }
        int touchCount { get; }

        float GetAxis( string axis, short playerID = 0 );
        bool GetButton( string button, short playerID = 0 );
        bool GetButtonUp( string button, short playerID = 0 );
        bool GetButtonDown( string button, short playerID = 0 );
        float GetAxisRaw( string axis, short playerID = 0 );
        bool GetMouseButtonDown( int button );
        Touch GetTouch( int index );
    }
}