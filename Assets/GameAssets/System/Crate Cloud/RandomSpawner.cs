﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      RandomSpawner.cs                       </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A random object spawner.               </summary>
    /// <remarks> 
    ///     The component will spawn random objects selected from 
    ///     a list at a random time and random position within the
    ///     bounds of the <c>Collider</c> of this GameObject.
    /// </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class RandomSpawner : NetworkBehaviour
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The minimal spawn time in seconds."                      )]
        //--------------------------------------
        float _minTime;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The maximal spawn time in seconds."                      )]
        //--------------------------------------
        float _maxTime;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "A list of objects to be randomly spawned."               )]
        //--------------------------------------
        NetworkIdentity[] _spawnObjects;
        #endregion
        //......................................


        Timer   _spawnTimer;
        Bounds  _spawnArea;


        //..............................................................
        #region               //  METHODS  //                
        //--------------------------------------------------------------
        /// <summary> Unity start callback (server only).     </summary>
        //--------------------------------------
        [ServerCallback]
        protected void Start()
        {
            _spawnTimer = new Timer();
            Collider collider = GetComponent<Collider>();

            if( collider )
                _spawnArea = collider.bounds;
        }



        //--------------------------------------------------------------
        /// <summary> Unity update callback (server only).    </summary>
        /// <remarks> This method does the actual spawning.   </remarks>
        //--------------------------------------
        [ServerCallback]
        protected void Update()
        {
            _spawnTimer.Step( Time.deltaTime );

            if( !_spawnTimer.isRunning )
            {
                _spawnTimer.Reset(  Random.Range( _minTime, _maxTime )  );

                NetworkIdentity spawnObj = RandomSpawnObject();

                if( spawnObj )
                {
                    var obj = Instantiate(
                                    spawnObj,
                                    RandomSpawnPosition(),
                                    Quaternion.identity ) as NetworkIdentity;

                    NetworkServer.Spawn( obj.gameObject );
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Returns a random position within the bounds 
        ///            of the collider of this GameObject.    </summary>
        //--------------------------------------
        private Vector3 RandomSpawnPosition()
        {
            Vector3 min = _spawnArea.min;
            Vector3 max = _spawnArea.max;

            return new Vector3( Random.Range( min.x, max.x ),
                                Random.Range( min.y, max.y ),
                                Random.Range( min.z, max.z ) );
        }



        //--------------------------------------------------------------
        /// <summary>  Returns a random object from the list. </summary>
        //--------------------------------------
        private NetworkIdentity RandomSpawnObject()
        {
            return _spawnObjects[Random.Range( 0, _spawnObjects.Length )];
        }
        #endregion
        //......................................
    }
}