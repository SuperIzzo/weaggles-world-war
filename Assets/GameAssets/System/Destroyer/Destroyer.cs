﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {

	void OnTriggerEnter( Collider col )
	{
		Destroy( col.gameObject );
	}
}
