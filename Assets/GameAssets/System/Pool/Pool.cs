﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    
    static class Pool
    {
        static Dictionary<GameObject, SinglePool> _pools;

        static public GameObject Instantiate(GameObject original, Vector3 position, Quaternion orientation )
        {
            if( _pools==null )
                _pools = new Dictionary<GameObject, SinglePool>();

            SinglePool pool = null;

            if( !original )
            {
                throw new ArgumentNullException( "original" );
            }

            if( _pools.ContainsKey(original) )
            {
                pool = _pools[original];
            }
            else
            {
                pool = new SinglePool( original );
                _pools[original] = pool;
            }

            if( pool!=null )
            {
                GameObject obj = pool.Instantiate(position, orientation);
                _pools[obj] = pool;
                return obj;
            }
            else
            {
                throw new Exception( "No pool exists or could be created for object "
                    + original + ". The pool container may be corrupted or new allocations fail." );
            }
        }

        internal static void Clear()
        {
            if( _pools!=null )
                _pools.Clear();
        }

        static public GameObject Instantiate( GameObject original )
        {
            return Instantiate(original, Vector3.zero, Quaternion.identity);
        }

        static public void Release(GameObject gameObject)
        {
            if( !gameObject )
            {
                throw new ArgumentNullException( "gameObject" );
            }

            if( _pools.ContainsKey(gameObject) )
            {
                SinglePool objectPool = _pools[gameObject];

                objectPool.Release( gameObject );
            }
            else
            {
                Debug.LogError( "Releasing an object without a pool." );
            }
        }


        private class SinglePool
        {
            GameObject       _prototype;
            HashSet<GameObject> _activeObjects;
            Queue<GameObject> _inactiveObjects;

            public SinglePool(GameObject prototype)
            {
                _prototype = prototype;
                _activeObjects = new HashSet<GameObject>();
                _inactiveObjects = new Queue<GameObject>();
            }

            public GameObject Instantiate(Vector3 position, Quaternion orientation)
            {
                GameObject obj = null;

                if( _inactiveObjects.Count > 0 )
                {
                    obj = _inactiveObjects.Dequeue();
                }
                else
                {
                    obj = GameObject.Instantiate( _prototype, position, orientation ) as GameObject;
                }

                if( obj )
                {
                    obj.transform.position = position;
                    obj.transform.rotation = orientation;

                    bool initHandled = false;
                    foreach( var poolable in obj.GetComponents<IPoolable>() )
                    {
                        poolable.OnPoolInit();
                        initHandled = true;
                    }

                    // Default pooled object behaviour
                    if( !initHandled )
                    {
                        obj.SetActive( true );
                    }

                    _activeObjects.Add( obj );
                }
                else
                {
                    Debug.LogError( "Instantiating a null object. This may happen if a pooled object has been inappropriatele destroyed instead of being released." );
                }

                return obj;
            }



            public void Release( GameObject obj )
            {
                
                if( _activeObjects.Contains( obj ) )
                {
                    _activeObjects.Remove( obj );
                }
                else
                {
                    Debug.LogWarning( "Releasing an object that has not been pooled." );
                }

                bool releaseHandled = false;
                foreach( var poolable in obj.GetComponents<IPoolable>() )
                {
                    poolable.OnPoolRelease();
                    releaseHandled = true;
                }

                // Default pooled object behaviour
                if( !releaseHandled )
                {
                    obj.SetActive( false );
                }
                
                _inactiveObjects.Enqueue( obj );                
            }
        }
    }
}
