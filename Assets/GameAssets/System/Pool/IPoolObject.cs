﻿namespace Izzo.WeagglesWorldWar
{
    public interface IPoolable
    {
        void OnPoolInit();
        void OnPoolRelease();
    }
}