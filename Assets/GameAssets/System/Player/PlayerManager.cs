﻿namespace Izzo.WeagglesWorldWar
{
    using System;
    using UnityEngine;



    public class PlayerManager : MonoBehaviour
    {
        [Serializable]
        struct PlayerData
        {
            public Color color;
            public GameHUD.ItemSlotPosition itemSlotHUD;
        }


        [SerializeField]
        PlayerData[] players;

        [SerializeField]
        PlayerData[] cpu;


        static PlayerManager _instance;
        public static PlayerManager instance
        {
            get
            {
                if( !_instance )
                    _instance = FindObjectOfType<PlayerManager>();

                return _instance;
            }
        }


        public static int maxNumberOfPlayers
        {
            get
            {
                if( instance )
                {
                    return instance.players.Length;
                }
                else
                {
                    Debug.LogError( "No instance of PlayerManager in the scene." );
                    return 0;
                }
            }
        }


        public static int maxNumberOfCPU
        {
            get
            {
                if( instance )
                {
                    return instance.cpu.Length;
                }
                else
                {
                    Debug.LogError( "No instance of PlayerManager in the scene." );
                    return 0;
                }
            }
        }


        public static GameHUD.ItemSlotPosition GetPlayerItemSlot( int playerID )
        {
            if( !instance )
            {
                Debug.LogError( "No instance of PlayerManager in the scene." );
                return GameHUD.ItemSlotPosition.None;
            }

            if( playerID >= 0 && playerID < instance.players.Length )
            {
                return instance.players[playerID].itemSlotHUD;
            }
            else
            {
                return GameHUD.ItemSlotPosition.None;
            }
        }


        public static Color GetPlayerColor( int playerID )
        {
            if( !instance )
            {
                Debug.LogError( "No instance of PlayerManager in the scene." );
                return Color.white;
            }

            if( playerID >= 0 && playerID < instance.players.Length )
            {
                return instance.players[playerID].color;
            }
            else
            {
                return Color.white;
            }
        }


        public static Color32 GetCPUColor( int cpuID )
        {
            if( !instance )
            {
                Debug.LogError( "No instance of PlayerManager in the scene." );
                return Color.white;
            }

            if( cpuID >= 0 && cpuID < instance.players.Length )
            {
                return instance.cpu[cpuID].color;
            }
            else
            {
                return Color.white;
            }
        }
    }
}