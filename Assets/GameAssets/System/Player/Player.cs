﻿namespace Izzo.WeagglesWorldWar
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Networking;


    [RequireComponent(typeof( PlayerTankController ))]
    public class Player : NetworkBehaviour
    {
        static List<Player> _players;

        [SyncVar]
        int _playerID;

        [SyncVar]
        TeamID     _teamID;

        GameObject tank;        
        IUnitSpawner spawnerBase;

        public TeamID teamID { get { return _teamID; } }

        public static List<Player> localPlayers
        {
            get
            {
                List<Player> players = new List<Player>();
                foreach( PlayerController controller in ClientScene.localPlayers )
                {
                    var player = controller.gameObject.GetComponent<Player>();
                    if( player )
                    {
                        players.Add( player );
                    }
                }
                return players;
            }
        }

        PlayerTankController _tankController;        

        PlayerTankController tankController
        {
            get
            {
                if( !_tankController )
                    _tankController = GetComponent<PlayerTankController>();

                return _tankController;
            }
        }
        


        // Use this for initialization        
        protected void Start()
        {
            if( isServer )
            {
                RegisterPlayer( this );

                UnitSpawner[] spawners = FindObjectsOfType<UnitSpawner>();
                foreach( IUnitSpawner spawner in spawners )
                {
                    if( spawner.spawnsPlayers )
                    {
                        spawnerBase = spawner;

                        if( spawnerBase is Component )
                        {
                            Team team = (spawnerBase as Component).GetComponent<Team>();
                            if( team )
                                _teamID = team.teamID;
                        }
                                                
                        break;
                    }
                }
            }
        }



        // Update is called once per frame
        [ServerCallback]
        protected void Update()
        {
            if( (!tank || !tank.activeInHierarchy) && spawnerBase!=null )
            {
                var spawnableComponent = spawnerBase.Spawn() as Component;

                if( spawnableComponent )
                    tank = spawnableComponent.gameObject;

                if( tank )
                {
                    WeaggleHardhat hat = tank.GetComponent<WeaggleHardhat>();
                    if( hat )
                    {
                        hat.playerID = _playerID;
                    }

                    ItemSlot itemSlot = tank.GetComponent<ItemSlot>();
                    if( itemSlot )
                    {
                        itemSlot.displayHUD = 
                            PlayerManager.GetPlayerItemSlot( _playerID );
                    }

                    // Locals will receive the RPC
                    if( !isClient )
                    {
                        TankSpawned( tank.GetComponent<NetworkIdentity>() );
                    }

                    RpcTankSpawned( tank.GetComponent<NetworkIdentity>() );
                }
            }            
        }



        [ClientRpc]
        private void RpcTankSpawned( NetworkIdentity tankNetId )
        {
            TankSpawned( tankNetId );
        }



        private void TankSpawned( NetworkIdentity tankNetId )
        {
            tank = tankNetId.gameObject;

            if( tankController )
                tankController.tankObject = tank;
        }



        private static void RegisterPlayer( Player playerObject )
        {
            if( _players == null )
                _players = new List<Player>();

            int idx = _players.FindIndex( player => !player );

            if( idx >= 0 )
            {
                _players[idx] = playerObject;
                playerObject._playerID = idx;
            }
            else
            {
                playerObject._playerID = _players.Count;
                _players.Add( playerObject );
            }
        }
    }
}