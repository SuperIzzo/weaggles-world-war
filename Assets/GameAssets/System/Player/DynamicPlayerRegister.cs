﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                  DynamicPlayerRegister.cs                   </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2014-2016  Hristoz Stefanov - All Rights Reserved          * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    04-Feb-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    public class DynamicPlayerRegister : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if( ClientScene.ready )
            {
                for( short i = 0; i < 4; i++ )
                {
                    if( InputManager.GetButtonDown( "Menu", i )
                        && !PlayerAdded( i ) )
                    {
                        ClientScene.AddPlayer( i );
                    }
                }
            }
        }


        private bool PlayerAdded( short id )
        {
            var player = ClientScene.localPlayers.Find(
            controller => controller.playerControllerId == id );

            return player != null && player.IsValid && player.gameObject;
        }
    }
}