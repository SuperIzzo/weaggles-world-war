﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                        TankMotor.cs                         </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using System;
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  Representation of the tank mobility systems. </summary>
    /// <remarks>
    /// 	This component is responsible for moving and turning the tank
    /// 	according to the game rules. It position and orientation snapping.
    /// </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class TankMotor : NetworkBehaviour
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The movement speed of the tank."                         )]
        //--------------------------------------
        float    _speed   = 10.0f;
                


        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The rotation speed of the tank."                         )]
        //--------------------------------------
        float    _turnSpeed = 50.0f;
        #endregion
        //......................................



        //..............................................................
        #region                  //  FIELDS  //
        //--------------------------------------------------------------
        /// <summary> The control direction. Used for input/ai. </summary>
        //--------------------------------------
        Vector3 _direction;



        //--------------------------------------------------------------
        /// <summary> The control direction. Used for input/ai. </summary>
        //--------------------------------------
        bool _updatePhysics;



        //--------------------------------------------------------------
        /// <summary> Movement gear. (1=forward, -1=backwards) </summary>
        //--------------------------------------
        float _gear  = 1.0f;



        //--------------------------------------------------------------
        /// <summary> Synchronized list of speed buffs. </summary>
        //--------------------------------------
        SyncListFloat _speedBuffs;


        
        //--------------------------------------------------------------
        /// <summary> Reference to the Rigidbody component. </summary>
        //--------------------------------------
        Rigidbody _rigidbody;
        #endregion
        //......................................



        //..............................................................
        #region                  //  METHODS  //
        //--------------------------------------------------------------
        /// <summary> Set up references. </summary>
        //--------------------------------------
        protected void Awake()
        {
            _speedBuffs = new SyncListFloat();
            _rigidbody = GetComponent<Rigidbody>();
        }



        //--------------------------------------------------------------
        /// <summary> Set up references. </summary>
        //--------------------------------------
        private float GetTotalSpeed()
        {
            float buffSum = 0;
            foreach( float buff in _speedBuffs )
            {
                buffSum += buff;
            }

            return _speed + buffSum;
        }



        //--------------------------------------------------------------
        /// <summary> Update physics. </summary>
        //--------------------------------------
        protected void FixedUpdate()
        {
            // Only update when we know 
            // where the tank is going.
            // _updatePhysics needs to be set only once
            if( !_updatePhysics )
                return;

            // Linear force
            float m = _rigidbody.mass;


            // Angular force
            Vector3 dirCross = Vector3.Cross(transform.forward, _direction.normalized);
            float   dirDot   = Vector3.Dot(transform.forward, _direction);

            _gear = Mathf.Sign( dirDot + 0.9f * _gear + 0.1f );

            float thetaSin = Mathf.Clamp01( dirCross.sqrMagnitude * 100 );
            float angle = Mathf.Min(thetaSin * _turnSpeed, _turnSpeed); //turnSpeed * theta;
            Vector3 w = dirCross.normalized * angle * _gear;// Time.fixedDeltaTime;

            Quaternion q = transform.rotation * _rigidbody.inertiaTensorRotation;
            Vector3 inertiaSpaceW = Quaternion.Inverse( q ) * w;
            Vector3 T = q * Vector3.Scale( _rigidbody.inertiaTensor, inertiaSpaceW );

            _rigidbody.AddTorque( T );


            float speed = GetTotalSpeed();
            Vector3 targetAcceleration = (_direction.normalized  + transform.forward*_gear)/2 * speed;

            Vector3 F = targetAcceleration * m;

            _rigidbody.AddForce( F );



            if( Mathf.Abs( _rigidbody.velocity.magnitude ) < 0.1f * speed )
            {
                _gear = 0.9f;
            }
        }



        //--------------------------------------------------------------
        /// <summary> Adds a buff to the motor. </summary>
        /// <param name="buff"> the added buff </param>
        //--------------------------------------
        [Server]
        public void AddBuff( Buff buff )
        {
            SpeedBuff speedBuff = buff as SpeedBuff;
            if( speedBuff!=null )
            {
                _speedBuffs.Add( speedBuff.addedSpeed );
            }
            else
            {
                throw new ArgumentException( "Motor cannot have buffs of type " + buff.GetType() );
            }            
        }        



        //--------------------------------------------------------------
        /// <summary> Adds a buff to the motor. </summary>
        /// <param name="Buff"> the added speed </param>
        /// <returns> The buff object </returns>
        //--------------------------------------
        [Server]
        public void RemoveBuff( Buff buff )
        {
            SpeedBuff speedBuff = buff as SpeedBuff;
            if( speedBuff != null )
            {
                _speedBuffs.Remove( speedBuff.addedSpeed );
            }
            else
            {
                throw new ArgumentException( "Motor cannot have buffs of type " + buff.GetType() );
            }
        }



        //--------------------------------------------------------------
        /// <summary> Move in the specified direction. </summary>
        /// <param name="direction"> the direction </param>
        //--------------------------------------
        public void Move( Vector3 dir )
        {
            _direction = dir;
            _updatePhysics = true;
        }      
        #endregion
        //......................................
    }
}
