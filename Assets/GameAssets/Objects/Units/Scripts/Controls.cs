﻿namespace Izzo.WeagglesWorldWar
{
    static internal class Controls
    {
        public const string  horizontal   = "Horizontal";
        public const string  vertical     = "Vertical";
        public const string  fire         = "Fire";
        public const string  item         = "Item";
        public const string  unit         = "Unit";
        public const string  explode      = "Explode";
        public const string  menu         = "Menu";
        public const string  accept       = "Accept";
        public const string  decline      = "Decline";
    }
}