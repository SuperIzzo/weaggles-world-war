﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                         ItemSlot.cs                         </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2015                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> A basic shooter that shoots projectiles. </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    // TODO: This (model) component is coupled with its view, 
    //       it needs redesigning
    public class ItemSlot : NetworkBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, SyncVar(hook = "SetDisplayHUD"), Tooltip
        (  "The HUD item slot position (either a corner or none)."    )]
        //--------------------------------------
        GameHUD.ItemSlotPosition _displayHUD;



        //--------------------------------------------------------------
        /// <summary>  The item type ID.                      </summary>
        //--------------------------------------
        [SyncVar]
        string _itemTypeID;



        //--------------------------------------------------------------
        /// <summary>  The amount of items of the given type. </summary>
        //--------------------------------------
        [SyncVar]
        int _number;



        //--------------------------------------------------------------
        /// <summary> An internal cache variable to hold the Item.</summary>
        //--------------------------------------
        Item _itemType_internal;



        //--------------------------------------------------------------
        /// <summary>  Gets the item held by this slot.            </summary>
        //--------------------------------------
        Item itemType
        {
            get
            {
                if( !_itemType_internal || _itemType_internal.name != _itemTypeID )
                {
                    if( _itemTypeID != null && _itemTypeID != "" )
                    {
                        _itemType_internal = ItemManager.FindItem( _itemTypeID );
                    }
                }

                return _itemType_internal;
            }
        }



        //--------------------------------------------------------------
        /// <summary> Get or sets the HUD position of this item slot.</summary>
        //--------------------------------------
        public GameHUD.ItemSlotPosition displayHUD
        {
            get { return _displayHUD; }
            set { SetDisplayHUD( value ); }
        }



        //--------------------------------------------------------------
        /// <summary>  true if this item slot has any items.  </summary>
        //--------------------------------------
        public bool isEmpty
        {
            get { return (itemType == null) || (_number <= 0); }
        }



        //--------------------------------------------------------------
        /// <summary>  Initializes the client.                </summary>
        //--------------------------------------
        public override void OnStartClient()
        {
            SetDisplayHUD( _displayHUD );
        }



        //--------------------------------------------------------------
        /// <summary>  Sets the item type and amount 
        ///            held by this item slot.                </summary>
        //--------------------------------------
        [Server]
        public void SetItem( Item itemType, int number = 1 )
        {
            _itemTypeID = itemType.name;
            _number = number;
        }



        //--------------------------------------------------------------
        /// <summary>   Returns the item type 
        ///             held by this item slot.               </summary>
        //--------------------------------------
        public Item GetItem()
        {
            return itemType;
        }



        //--------------------------------------------------------------
        /// <summary>   Returns the amount of items 
        ///             held by this item slot.               </summary>
        //--------------------------------------
        public int GetItemCount()
        {
            return _number;
        }



        //--------------------------------------------------------------
        /// <summary> Uses up a single item of the held item type.</summary>
        //--------------------------------------
        [Server]
        public void UseItem()
        {
            if( !isEmpty )
            {
                --_number;
                itemType.Use( gameObject );
            }
        }        



        //--------------------------------------------------------------
        /// <summary>  Sets up the the item HUD
        ///            based on its new HUD position.         </summary>
        //--------------------------------------
        private void SetDisplayHUD( GameHUD.ItemSlotPosition newDisplayHUD )
        {
            GameHUD hud = GameGUI.instance.hud;

            if( hud )
            {
                // Clear the previous slot if necessary
                if( _displayHUD != GameHUD.ItemSlotPosition.None )
                {
                    ItemSlotHUD prevItemHUD = hud.GetItemSlot( _displayHUD );

                    if( prevItemHUD.itemSlot == this )
                        prevItemHUD.itemSlot = null;
                }

                // Set the new slot if any
                if( newDisplayHUD != GameHUD.ItemSlotPosition.None )
                {
                    ItemSlotHUD newItemHUD = hud.GetItemSlot( newDisplayHUD );
                    newItemHUD.itemSlot = this;
                }
            }

            _displayHUD = newDisplayHUD;
        }
    }
}