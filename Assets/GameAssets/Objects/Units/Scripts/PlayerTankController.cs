﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                   PlayerTankController.cs                   </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2014-2016  Hristoz Stefanov - All Rights Reserved          * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> Controls the tank based on player input </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class PlayerTankController : NetworkBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The cooldown of the turbo shot."                         )]
        //--------------------------------------
        private Timer _turboCooldown = new Timer( 0.4f );



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The threshold after which we send movement commands."    )]
        //--------------------------------------
        private float _movementThreshold = 0.5f;



        //--------------------------------------------------------------
        [SerializeField, Range(0.01f, 30), Tooltip
        ( "Movement commands per second." )]
        //--------------------------------------
        private float _movementSendRate = 8;



        public GameObject tankObject { get; set; }


        private Vector2 _prevControlVector;
        private Vector2 _controlVectorBuffered;
        private float   _movementSendTime = 0;


        private TankMotor motor
        {
            get
            {
                return (tankObject && tankObject.activeSelf) ?
                    tankObject.GetComponent<TankMotor>() : null;
            }
        }


        private Shooter shooter
        {
            get
            {
                return (tankObject && tankObject.activeSelf) ?
                    tankObject.GetComponent<Shooter>() : null;
            }
        }


        private ItemSlot itemSlot
        {
            get
            {
                return (tankObject && tankObject.activeSelf) ?
                    tankObject.GetComponent<ItemSlot>() : null;
            }
        }


        private ExplodingWeaggle weaggle
        {
            get
            {
                return (tankObject && tankObject.activeSelf) ?
                    tankObject.GetComponent<ExplodingWeaggle>() : null;
            }
        }


        private IUnitSpawner spawner
        {
            get
            {
                if( tankObject && tankObject.activeSelf )
                {
                    var spawnable = tankObject.GetComponent<ISpawnableUnit>();

                    if( spawnable != null )
                        return spawnable.spawner;
                }

                return null;
            }
        }


        //--------------------------------------------------------------
        /// <summary> Update the object based on input </summary>
        //--------------------------------------
        void Update()
        {
            if( GameGUI.instance.currentDialog == null )
            {
                // Only update this for the local player
                // other players are handled by the network manager
                if( isLocalPlayer )
                {
                    HandleMovement();
                    HandleShooting();
                    HandleItems();
                    HandleReinforcements();
                    HandleExploding();
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles the movement based on player input. </summary>
        //--------------------------------------
        private void HandleMovement()
        {
            if( motor )
            {
                float xMove = InputManager.GetAxis (
                    Controls.horizontal, playerControllerId );

                float zMove = InputManager.GetAxis (
                    Controls.vertical, playerControllerId );

                // Construct a 3D vector from the input
                Vector2 controlVector   = new Vector2(xMove, zMove);

                // Do some buffering
                _movementSendTime += Time.deltaTime;
                _controlVectorBuffered += controlVector * _movementSendRate * Time.deltaTime;

                if( _movementSendTime >= 1 / _movementSendRate )
                {                    
                    _controlVectorBuffered /= _movementSendRate * _movementSendTime;


                    float controlDistance = 0;                    
                    if( _controlVectorBuffered.sqrMagnitude == 0
                        && _prevControlVector.sqrMagnitude != 0 )
                    {
                        // Make sure we always stop
                        controlDistance = 2;
                    }
                    else
                    {
                        controlDistance = 
                            (_controlVectorBuffered - _prevControlVector).magnitude;
                    }

                    if( controlDistance > _movementThreshold )
                    {
                        _prevControlVector = _controlVectorBuffered;
                        CmdMove( _controlVectorBuffered );

                        if( !isServer )
                            DoMove( _controlVectorBuffered );
                    }

                    _movementSendTime = 0;
                    _controlVectorBuffered = Vector2.zero;
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles the movement on the server. </summary>
        //--------------------------------------
        [Command]
        private void CmdMove( Vector2 controlVector )
        {
            DoMove( controlVector );
        }



        //--------------------------------------------------------------
        /// <summary> Actually moves the local copy of the tank. </summary>
        //--------------------------------------
        private void DoMove( Vector2 controlVector )
        {            
            if( motor )
            {
                motor.Move( new Vector3( controlVector.x, 0, controlVector.y ) );
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles the shooting based on player input. </summary>
        //--------------------------------------
        private void HandleShooting()
        {
            if( shooter )
            {
                // Single keypress invokes a fire event
                if( InputManager.GetButtonDown( Controls.fire, playerControllerId ) )
                {
                    CmdFire();
                }

                // If the button is held down we invoke a fire event
                // on regular basis, however there is a short delay
                // between each shot (turboCountdown). This is so to
                // reward quick spamming button presses
                _turboCooldown.Step( Time.deltaTime );
                if( InputManager.GetButton( Controls.fire, playerControllerId ) )
                {
                    if( !_turboCooldown.isRunning )
                    {
                        CmdFire();
                        _turboCooldown.Reset();
                    }
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles the shooting on the server. </summary>
        //--------------------------------------
        [Command]
        private void CmdFire()
        {
            if( shooter )
            {
                shooter.Fire();
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles item usage based on player input. </summary>
        //--------------------------------------
        private void HandleItems()
        {
            if( itemSlot && !itemSlot.isEmpty )
            {
                if( InputManager.GetButtonDown( Controls.item, playerControllerId ) )
                {
                    CmdUseItem();
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles item usage on the server. </summary>
        //--------------------------------------
        [Command]
        private void CmdUseItem()
        {
            if( itemSlot )
            {
                itemSlot.UseItem();
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles summoning based on player input. </summary>
        //--------------------------------------
        private void HandleReinforcements()
        {
            if( InputManager.GetButtonDown( Controls.unit, playerControllerId ) )
            {
                if( spawner != null )
                {
                    var spawnerComponent = spawner as Component;
                    if( spawnerComponent )
                    {
                        IUnitSpawner[] spawners = spawnerComponent.GetComponents<IUnitSpawner>();
                        foreach( IUnitSpawner aspawner in spawners )
                        {
                            if( aspawner != spawner )
                                aspawner.Spawn();
                        }
                    }
                }

            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles exploding based on player input. </summary>
        //--------------------------------------
        private void HandleExploding()
        {
            bool charging = false;
            bool changed = false;

            if( InputManager.GetButtonDown( Controls.explode, playerControllerId ) )
            {
                charging = true;
                changed = true;
            }

            if( InputManager.GetButtonUp( Controls.explode, playerControllerId ) )
            {
                charging = false;
                changed = true;
            }

            if( changed )
            {
                CmdWeaggle( charging );
            }
        }



        //--------------------------------------------------------------
        /// <summary> Handles exploding on the server. </summary>
        //--------------------------------------
        [Command]
        private void CmdWeaggle( bool charging )
        {
            if( weaggle )
            {
                weaggle.chargingDetonation = charging;
            }
        }
    }
}