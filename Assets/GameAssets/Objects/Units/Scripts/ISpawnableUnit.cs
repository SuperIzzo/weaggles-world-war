﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      ISpawnableUnit.cs                      </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    25-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    public delegate void SpawnableUnitDestroyedDelegate( ISpawnableUnit obj );


    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>   A general spawnable unit interface.   </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public interface ISpawnableUnit
    {
        //--------------------------------------------------------------
        /// <summary> Gets the cost of the unit.              </summary>
        //--------------------------------------
        float cost { get; }


        //--------------------------------------------------------------
        /// <summary> Gets the upkeep of the unit.            </summary>
        //--------------------------------------
        float upkeep { get; }


        //--------------------------------------------------------------
        /// <summary> Gets the spawner that spawned the unit. </summary>
        //--------------------------------------
        IUnitSpawner spawner { get; }


        //--------------------------------------------------------------
        /// <summary>  Event called upon unit destruction.    </summary>
        /// <remarks>  The spawner will register to this event so that
        ///            it can release the held upkeep and any 
        ///            other resources.                       </remarks>
        //--------------------------------------
        event SpawnableUnitDestroyedDelegate EventDestroyed;


        //--------------------------------------------------------------
        /// <summary>  Spawn callback from the spawner.       </summary>
        /// <remarks>  This method should be called only once 
        ///            and only from the spawner when the unit 
        ///            is being spawned.                      </remarks>
        //--------------------------------------
        void OnSpawn( IUnitSpawner spawnerBase );
    }
}