﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      SpawnableUnit.cs                       </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;

    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A basic spawnable unit implementation. </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=                  
    public class SpawnableUnit : MonoBehaviour, ISpawnableUnit
    {        
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The resource cost of the unit."                          )]
        //--------------------------------------
        float _cost;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The upkeep cost of the unit."                            )]
        //--------------------------------------
        float _upkeep;


        //--------------------------------------------------------------
        /// <summary>  Event called upon unit destruction.    </summary>
        //--------------------------------------
        public event SpawnableUnitDestroyedDelegate EventDestroyed;


        //--------------------------------------------------------------
        /// <summary> Gets the cost of the unit.              </summary>
        //--------------------------------------
        public float cost { get { return _cost; } }


        //--------------------------------------------------------------
        /// <summary> Gets the upkeep of the unit.            </summary>
        //--------------------------------------
        public float upkeep { get { return _upkeep; } }


        //--------------------------------------------------------------
        /// <summary> Gets the spawner that spawned the unit. </summary>
        //--------------------------------------
        public IUnitSpawner spawner { get; private set; }


        //--------------------------------------------------------------
        /// <summary>  Spawn callback from the spawner.       </summary>
        //--------------------------------------
        public void OnSpawn( IUnitSpawner spawnerBase )
        {
            this.spawner = spawnerBase;
        }


        //--------------------------------------------------------------
        /// <summary>  Unity callback; fires EventDestroyed.  </summary>
        //--------------------------------------
        protected void OnDestroy()
        {
            if( EventDestroyed!=null )
                EventDestroyed(this);
        }
    }
}