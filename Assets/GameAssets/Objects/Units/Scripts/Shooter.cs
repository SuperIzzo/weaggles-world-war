﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                         Shooter.cs                          </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2015                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> A basic shooter that shoots projectiles. </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class Shooter : NetworkBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        ("The projectile type, must contain a IProjectileType component.")]
        //--------------------------------------
        GameObject _projectileType;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (  "The spawn point for bullets."                             )]
        //--------------------------------------
        Transform _projectileSpawnPoint;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (  "The cooldown in between shots (in seconds)."              )]
        //--------------------------------------
        float _shotCooldown = 0.3f;



        //--------------------------------------------------------------
        /// <summary>  Shot cooldown timer.                   </summary>
        //--------------------------------------
        float _shotTimer;


        //--------------------------------------------------------------
        /// <summary> The projectile type this shooter shoots.</summary>
        //--------------------------------------
        IProjectileType projectileType { get; set; }



        //--------------------------------------------------------------
        /// <summary>  Initializes the component.             </summary>
        //--------------------------------------
        protected void Awake()
        {
            projectileType = _projectileType.GetComponent<IProjectileType>();

            if( _projectileType  &&  projectileType==null )
            {
                Debug.LogError( "'" + _projectileType.name +
                    "' does not have a IProjectileType component." );
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Updates the component.                 </summary>
        //--------------------------------------
        void Update()
        {
            if( isServer && _shotTimer > 0 )
            {
                _shotTimer -= Time.deltaTime;
            }            
        }



        //--------------------------------------------------------------
        /// <summary>  Fires a projectile.                    </summary>
        /// <remarks>
        /// 	This function is invoked from a controlling component 
        ///     such a player controller or an AI ontroller. 
        ///     The shooting rate is determined by the cooldown field.
        ///     If a fire event is invoked while the cooldown has not
        ///     charged, the event is ignored. On successful completion
        ///     the method creates an instance of the specified 
        ///     projectile type, places it and orients it appropriately.
        ///                                                   </remarks>
        //--------------------------------------
        [Server]
        public void Fire()
        {
            if( _shotTimer<=0  &&  projectileType!=null )
            {
                _shotTimer = _shotCooldown;

                projectileType.ShootNew( 
                    this,
                    _projectileSpawnPoint.position,
                    _projectileSpawnPoint.rotation );
            }
        }
    }
}