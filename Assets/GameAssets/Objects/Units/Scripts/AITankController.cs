﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;


    [System.Serializable]
    public struct TargetBehaviour
    {
        public float random;
        public float enemyUnit;
        public float enemyBase;
        public float allyUnit;
        public float allyBase;
        public float crate;
        public float pickup;
    }



    public class AITankController : NetworkBehaviour
    {
        // Configuration
        public float redirectionFrequency;
        [Range(0,1)]
        public float redirectionRatio = 1;
        public float shootFrequency;
        public float itemFrequency;

        public TargetBehaviour target;
        private Team team;
        private GameController game = null;

        private Vector2 moveDirection;
        private static GameObject[] units;
        private static GameObject[] spawners;
        private static GameObject[] crates;
        private static GameObject[] pickups;

        private delegate void UpdateFunc();
        private delegate bool TestFunc( GameObject obj );

        private float aiFixedDelta = 0.3f;
        private float aiUpdateTime = 0;



        public override void OnStartServer()
        {
            WeaggleHardhat hat = GetComponent<WeaggleHardhat>();

            if( hat )
                hat.cpuID = 0;
        }


        [ServerCallback]
        void Start()
        {
            team = GetComponent<Team>();            

            if( units == null )
                UpdateUnits();

            if( spawners == null )
                UpdateSpawners();

            if( crates == null )
                UpdateCrates();

            if( pickups == null )
                UpdatePickups();
        }



        [ServerCallback]
        void Update()
        {
            aiUpdateTime += Time.deltaTime;

            // Frame independent ai update loop
            // we do it this way because it's easier to use Random.value
            for( ; aiUpdateTime > aiFixedDelta; aiUpdateTime -= aiFixedDelta )
            {
                UpdateAI();
            }
        }



        // Update is called once per frame
        void UpdateAI()
        {
            // Don't do anything while the game is paused
            if( game != null && game.isPaused )
                return;


            // Randomly change the move direction 
            // every now and then based on redirectionFrequency
            if( Random.value / aiFixedDelta < redirectionFrequency )
            {
                moveDirection = GetMoveDirection();
                moveDirection.Normalize();
            }

            // Move based on the set moveDirection
            Vector3 controlVector   = new Vector3(  moveDirection.x * redirectionRatio,
                                                0,
                                                moveDirection.y * redirectionRatio);
            Vector3 currentDirection = GetComponent<Rigidbody>().velocity.normalized;
            currentDirection.y = 0;

            controlVector += currentDirection * (1 - redirectionRatio);

            if( controlVector.magnitude > 0 )
            {
                GetComponent<TankMotor>().Move( controlVector );
            }

            // Shoot randomly based on shootFrequency 
            if( Random.value / aiFixedDelta < shootFrequency )
            {
                GetComponent<Shooter>().Fire();
            }

            if( Random.value / aiFixedDelta < itemFrequency )
            {
                ItemSlot itemSlot = GetComponent<ItemSlot>();
                if( itemSlot )
                    itemSlot.UseItem();
            }
        }



        Vector2 GetMoveDirection()
        {
            // Initialize to zero,
            // if everything should fail, we fall back to not moving
            Vector3 randomVec       = Vector3.zero;
            if( target.random != 0 )
            {
                randomVec = new Vector3( Random.value - 0.5f, 0, Random.value - 0.5f );
                randomVec.Normalize();
                randomVec *= target.random * Random.value;
            }

            Vector3 result = randomVec
        +   ChoseRandomPosition( target.enemyUnit,  units,      IsEnemy, UpdateUnits )
        +   ChoseRandomPosition( target.enemyBase,  spawners,   IsEnemy, UpdateSpawners )
        +   ChoseRandomPosition( target.allyUnit,   units,      IsAlly,  UpdateUnits )
        +   ChoseRandomPosition( target.allyBase,   spawners,   IsAlly,  UpdateSpawners )
        +   ChoseRandomPosition( target.crate,      crates,     null,    UpdateCrates )
        +   ChoseRandomPosition( target.pickup,     pickups,    null,    UpdatePickups );

            Vector2 endDirection2D = new Vector2( result.x, result.z );
            endDirection2D.Normalize();

            // Return the normalized XZ 2D vector
            return endDirection2D;
        }



        Vector3 ChoseRandomPosition( float weight, GameObject[] objects, TestFunc test, UpdateFunc update )
        {
            if( weight != 0 )
            {
                int startIdx = Random.Range( 0, objects.Length );
                for( int i = 0; i < objects.Length; i++ )
                {
                    // Randomly offset and wrap around our index 
                    int objIndex = (i+startIdx)%objects.Length;

                    GameObject obj = objects[objIndex];
                    if( obj )
                    {
                        if( test == null || test( obj ) )
                        {
                            Vector3 result = obj.transform.position - transform.position;
                            return result.normalized * weight * Random.value;
                        }
                    }
                    else
                    {
                        // Woops a null reference,
                        // That means we are outdated
                        if( update != null )
                        {
                            update();
                        }

                        return Vector3.zero;
                    }
                }
            }

            return Vector3.zero;
        }



        bool IsAlly( GameObject obj )
        {
            Team objTeam = obj.GetComponent<Team>();
            return Team.IsAlly( team, objTeam );
        }



        bool IsEnemy( GameObject obj )
        {
            Team objTeam = obj.GetComponent<Team>();
            return !Team.IsAlly( team, objTeam );
        }



        static void UpdateUnits()
        {
            units = GameObject.FindGameObjectsWithTag( "Unit" );
        }



        static void UpdateSpawners()
        {
            spawners = GameObject.FindGameObjectsWithTag( "Spawner" );
        }



        static void UpdateCrates()
        {
            crates = GameObject.FindGameObjectsWithTag( "Crate" );
        }



        static void UpdatePickups()
        {
            pickups = GameObject.FindGameObjectsWithTag( "Pickup" );
        }
    }
}