﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                           Bomb.cs                           </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    10-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  Bomb entity script.                    </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class Bomb : NetworkBehaviour, IHealthListener
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The minimal time before detonating the bomb."            )]
        //--------------------------------------
        float _minDetonationTime = 7.0f;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The maximal time before detonating the bomb."            )]
        //--------------------------------------
        float _maxDetonationTime = 20.0f;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The remaining time when the bomb will start turning red.")]
        //--------------------------------------
        float _colorizationTime = 4;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The remaining time when the bomb will start turning red " +
            "after it takes damage."                                  )]
        //--------------------------------------
        float _hitColorizationTime = 1;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The explosion generated when the bomb goes off."         )]
        //--------------------------------------
        GameObject _explosion;
        #endregion
        //......................................



        //..............................................................
        #region                 //  FIELDS  //
        //--------------------------------------------------------------
        /// <summary>  Detonation countdown timer.            </summary>
        //--------------------------------------
        [SyncVar]
        float _detonationTimer;


        //--------------------------------------------------------------
        /// <summary>  Reference to the Renderer component.   </summary>
        //--------------------------------------
        Renderer _renderer;


        //--------------------------------------------------------------
        /// <summary>  The original tint color.               </summary>
        //--------------------------------------
        Color _originalColor;


        //--------------------------------------------------------------
        /// <summary>  Fully charged bomb tint color.         </summary>
        //--------------------------------------
        private static readonly Color tintColor = Color.red;
        #endregion
        //......................................



        //..............................................................
        #region               //  METHODS  // 
        //--------------------------------------------------------------
        /// <summary>  Initializes the component.             </summary>
        //--------------------------------------
        void Start()
        {
            if( isServer )
            {
                _detonationTimer = Random.Range( _minDetonationTime, _maxDetonationTime );
                
            }

            if( isClient )
            {
                _renderer = GetComponent<Renderer>();
                if( _renderer )
                {
                    _originalColor = _renderer.material.color;
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Updates the component.                 </summary>
        //--------------------------------------
        void Update()
        {
            if( isServer )
            {
                _detonationTimer -= Time.deltaTime;

                if( _detonationTimer <= 0 )
                {
                    Detonate();
                }
            }

            if( isClient )
            { 
                if( _renderer  &&  (_detonationTimer < _colorizationTime) )
                {
                    float t = 
                        (_colorizationTime-_detonationTimer)/_colorizationTime;

                    _renderer.material.color = 
                        Color.Lerp(  _originalColor, 
                                     tintColor,
                                     Mathf.Clamp01( t ));
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Health event callback.                 </summary>
        //--------------------------------------
        void IHealthListener.OnHealthChanged( 
            object sender, 
            float currentHealth, 
            float prevHealth )
        {
            if( isServer
                && currentHealth < prevHealth  // i.e. we've taken damage
                && _detonationTimer > _colorizationTime
                && _hitColorizationTime > 0 )
            {
                _colorizationTime = _hitColorizationTime;
                _detonationTimer = _hitColorizationTime;
            }
            
        }



        //--------------------------------------------------------------
        /// <summary>  Go BOOM!!!                             </summary>
        //--------------------------------------
        [Server]
        void Detonate()
        {
            var explosion = Instantiate( 
                _explosion, 
                transform.position, 
                Quaternion.identity ) as GameObject;

            NetworkServer.Spawn( explosion );

            Destroy( gameObject );            
        }
        #endregion
        //......................................
    }
}