﻿using UnityEngine;
using UnityEngine.Networking;

public class ExplosionForce : MonoBehaviour
{
    [SerializeField]
    float _power = 0;



    [SerializeField]
    float _radius = 0;



    void OnTriggerEnter( Collider collider )
    {        
        var rigidBody = collider.GetComponent<Rigidbody>();
        if( rigidBody )
        {
            rigidBody.AddExplosionForce( _power, transform.position, _radius );
        }
    }

}
