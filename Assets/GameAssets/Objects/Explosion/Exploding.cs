﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;


    public class Exploding : NetworkBehaviour, IHealthListener
    {
        public GameObject explosion;
        public GameObject underwaterExplosion;
        private bool _inWater;

        [ServerCallback]
        public void OnHealthChanged( object sender, float current, float prev )
        {            
            if( current<=0 )
            {
                GameObject explosionInstance = null;
                if( _inWater )
                {
                    // TODO: Make the test on location rather than damage type
                    explosionInstance = 
                        Instantiate( underwaterExplosion, transform.position, Quaternion.identity ) 
                        as GameObject;
                }
                else
                {
                    explosionInstance = 
                        Instantiate( explosion, transform.position, transform.rotation )
                        as GameObject;
                }

                NetworkServer.Spawn( explosionInstance );
            }
        }

        protected void OnTriggerEnter( Collider col )
        {
            if( col.tag == Tags.Water )
            {
                _inWater = true;
            }
        }

        protected void OnTriggerExit( Collider col )
        {
            if( col.tag == Tags.Water )
            {
                _inWater = false;
            }
        }
    }
}