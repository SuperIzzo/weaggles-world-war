﻿using UnityEngine;
using System.Collections;

public class ForceBall : MonoBehaviour
{
    [SerializeField]
    float _power = 100;

    [SerializeField]
    float _radius = 1;


    //float _antigrav = 1;


    Rigidbody _rigidbody;


	// Use this for initialization
	void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //float cycle = Mathf.PingPong(Time.fixedTime/2,1)*0.6f + 0.5f; ;// + 0.4f;
        //Random.Range(-0.5f, 0.3f) + 1f ;
        //_antigrav = _antigrav * 0.9f + cycle * 0.1f;

         _rigidbody.AddForce( -Physics.gravity * 0.2f , ForceMode.Acceleration );

        /*
        Ray ray = new Ray( transform.position, Physics.gravity );
        RaycastHit info;

        if( Physics.Raycast( ray, out info, 2.0f ) )
        {
            _rigidbody.AddForce( 
                -Physics.gravity * (1-info.distance)* Mathf.Sin(Time.fixedTime)
                , ForceMode.Acceleration);
        }
        */
        

        Collider[] colliders = Physics.OverlapSphere( transform.position, _radius );
        foreach( var collider in colliders )
        {
            Rigidbody rigidBody = collider.GetComponent<Rigidbody>();

            if( rigidBody && rigidBody!=_rigidbody )
            {
                rigidBody.AddExplosionForce( _power, transform.position, _radius );
            }
        }
	}
}
