﻿using UnityEngine;
using System.Collections;

public class SetupCrateAnim : MonoBehaviour
{
    private Transform _parent;
    private bool      _initialized = false;

	// Use this for initialization
	void Start ()
    {
        // Make is the parent transform
        _initialized = true;
        _parent = transform.parent;        
        if( _parent )
        {
            transform.parent = null;
            _parent.parent = transform;
        }
	}

    void Update()
    {
        if( _initialized && (!_parent || !_parent.gameObject.activeSelf) )
        {
            Destroy( gameObject );
        }
    }
}
