﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;
    using System.Collections.Generic;
    

    public class CrateDrop : NetworkBehaviour, IHealthListener
    {
        static bool isShuttingDown = false;
        public List<GameObject> drops;

        void Start()
        {
            if( drops == null )
            {
                drops = new List<GameObject>();
            }
        }


        void OnApplicationQuit()
        {
            isShuttingDown = true;
        }


        public void OnHealthChanged( object sender, float current, float prev )
        {
            if( current<=0 && !isShuttingDown && hasAuthority )
            {
                int dropIdx = Random.Range( 0, drops.Count );
                GameObject dropPrefab = drops[dropIdx];
                if( dropPrefab )
                {
                    var drop = Instantiate( dropPrefab, 
                                            transform.position, 
                                            Quaternion.identity) as GameObject;

                    NetworkServer.Spawn( drop );
                }
            }
        }
    }
}