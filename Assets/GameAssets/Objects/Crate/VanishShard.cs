﻿using UnityEngine;
using System.Collections;

public class VanishShard : MonoBehaviour
{
    float timeToVanish = 8.0f;
    float vanishTimer;

    // Use this for initialization
    void Start ()
    {
        vanishTimer = timeToVanish;
    }
	
	// Update is called once per frame
	void Update ()
    {
        vanishTimer -= Time.deltaTime;

        if( vanishTimer <= 0 )
        {
            GameObject.Destroy( transform.parent.gameObject );
        }
        else
        {
            transform.localScale = (1-Mathf.Pow(1-vanishTimer/timeToVanish,3)) * Vector3.one;
        }
	}
}
