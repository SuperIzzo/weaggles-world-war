﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                         HatList.cs                          </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    28-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;


    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> A list of weaggle hats.                 </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class HatList : ScriptableObject
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The default hat mesh."                                   )]
        //--------------------------------------
        Mesh _defaultHat;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "A list of player hat meshes."                            )]
        //--------------------------------------
        Mesh[] _playerHats;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "A list of CPU hat meshes."                               )]
        //--------------------------------------
        Mesh[] _cpuHats;
        #endregion
        //......................................      



        //..............................................................
        //  Generated mesh cache
        //--------------------------------------------------------------
        Mesh   _coloredDefaultHat;
        Mesh[] _coloredPlayerHats;
        Mesh[] _coloredCpuHats;
        //......................................    



        //..............................................................
        #region                //  METHODS  //
        //--------------------------------------------------------------
        /// <summary>  Returns a configured hat mesh for 
        ///            the specified player or CPU.           </summary>
        /// <param name="number"> Player or CPU id.             </param>
        /// <param name="player"> Player if true; CPU otherwise.</param>
        /// <returns> The appropriate mesh for the parameters.</returns>
        /// <remarks>
        ///     If there is no appropriate configuration this method 
        ///     will return the default mesh colored in the default
        ///     color. A negative number will always return the 
        ///     default.                                      </remarks>
        //--------------------------------------
        public Mesh GetHat( int number, bool player = true )
        {
            if( number < 0 )
                return ColoredDefaultHat();

            if( player && _playerHats != null )
            {
                return ColoredPlayerHat( number );
            }
            else if( !player && _cpuHats != null )
            {
                return ColoredCpuHat( number );
            }
            else
            {
                return ColoredDefaultHat();
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Generates and returns a player hat.    </summary>
        /// <param name="idx"> Player id.                       </param>
        //--------------------------------------
        Mesh ColoredPlayerHat( int idx )
        {
            int size = Mathf.Max( _playerHats.Length, PlayerManager.maxNumberOfPlayers );

            if( idx >= size )
            {
                return ColoredDefaultHat();
            }

            if( _coloredPlayerHats == null || _coloredPlayerHats.Length < size )
            {
                _coloredPlayerHats = new Mesh[size];
            }

            if( !_coloredPlayerHats[idx] )
            {
                Color32 col = PlayerManager.GetPlayerColor( idx );
                Mesh mesh = null;

                if( idx < _playerHats.Length )
                    mesh = _playerHats[idx];

                if( !mesh )
                    mesh = _defaultHat;

                _coloredPlayerHats[idx] = MeshUtil.ColoredMesh( mesh, col, " P" + idx );
            }

            return _coloredPlayerHats[idx];
        }



        //--------------------------------------------------------------
        /// <summary>  Generates and returns a CPU hat.       </summary>
        /// <param name="idx"> CPU id.                          </param>
        //--------------------------------------
        Mesh ColoredCpuHat( int idx )
        {
            int size = Mathf.Max( _cpuHats.Length, PlayerManager.maxNumberOfCPU );

            if( idx >= size )
            {
                return ColoredDefaultHat();
            }

            if( _coloredCpuHats == null || _coloredCpuHats.Length < size )
            {
                _coloredCpuHats = new Mesh[size];
            }

            if( !_coloredCpuHats[idx] )
            {
                Mesh mesh = null;
                Color32 col = PlayerManager.GetCPUColor( idx );

                if( idx < _cpuHats.Length )
                    mesh = _cpuHats[idx];

                if( !mesh )
                    mesh = _defaultHat;

                _coloredCpuHats[idx] = MeshUtil.ColoredMesh( mesh, col, " P" + idx );
            }

            return _coloredCpuHats[idx];
        }



        //--------------------------------------------------------------
        /// <summary>  Generates and returns a default hat.   </summary>
        /// <param name="idx"> Player id.                       </param>
        //--------------------------------------
        Mesh ColoredDefaultHat()
        {
            if( !_coloredDefaultHat )
            {
                _coloredDefaultHat = MeshUtil.ColoredMesh( _defaultHat, 
                                                           Color.white, 
                                                           " Default" );
            }

            return _coloredDefaultHat;
        }
        #endregion
        //......................................
    }
}