﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      WeaggleHardhat.cs                      </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    28-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary> A weaggle hat changer component.        </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class WeaggleHardhat : NetworkBehaviour
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The MeshFilter for the hat object."                      )]
        //--------------------------------------
        MeshFilter _hatMeshFilter;

        
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The HatList configuration file."                         )]
        //--------------------------------------
        HatList    _hatList;
        #endregion
        //......................................


        //..............................................................
        #region                //  STATE  //
        //--------------------------------------------------------------
        /// <summary>  The id of the player or CPU.           </summary>
        //--------------------------------------
        [SyncVar(hook ="SetHatID")]
        int _hatID = -1;


        //--------------------------------------------------------------
        /// <summary>  Whether this weaggles is a player.     </summary>
        //--------------------------------------
        [SyncVar(hook ="SetIsPlayer")]
        bool _isPlayer = true;
        #endregion
        //......................................



        //..............................................................
        #region             //  PUBLIC PROPERTIES  //
        //--------------------------------------------------------------
        /// <summary>  Gets or sets the player ID.            </summary>
        /// <remarks>  Setting this will set cpuID to -1.     </remarks>
        //--------------------------------------
        public int playerID
        {
            get
            {
                return _isPlayer ? _hatID : -1;
            }

            set
            {
                _hatID = value;
                _isPlayer = true;
            }
        }


        //--------------------------------------------------------------
        /// <summary>  Gets or sets the CPU ID.               </summary>
        /// <remarks>  Setting this will set playerID to -1.  </remarks>
        //--------------------------------------
        public int cpuID
        {
            get
            {
                return _isPlayer ? -1 : _hatID;
            }

            set
            {
                _hatID = value;
                _isPlayer = false;
            }
        }


        //--------------------------------------------------------------
        /// <summary>  Gets whether this is a player or CPU.  </summary>
        //--------------------------------------
        public bool isPlayer
        {
            get { return _isPlayer; }
        }
        #endregion
        //......................................



        //..............................................................
        #region                 //  METHODS  //
        //--------------------------------------------------------------
        /// <summary> NetworkBehaviour callback. Resets the hat. </summary>
        //--------------------------------------
        public override void OnStartClient()
        {
            SetHat(_hatID, _isPlayer);
        }



        //--------------------------------------------------------------
        /// <summary> _hatID hook. Resets the hat. </summary>
        //--------------------------------------
        private void SetHatID( int number )
        {
            SetHat( number, _isPlayer );
        }



        //--------------------------------------------------------------
        /// <summary> _isPlayer hook. Resets the hat. </summary>
        //--------------------------------------
        private void SetIsPlayer( bool player )
        {
            SetHat( _hatID, player );
        }



        //--------------------------------------------------------------
        /// <summary> Sets the hat mesh from the <c>hatList</c>. </summary>
        //--------------------------------------
        private void SetHat( int hatID, bool player )
        {
            _hatMeshFilter.mesh = _hatList.GetHat( hatID, player );
        }
        #endregion
        //......................................
    }
}