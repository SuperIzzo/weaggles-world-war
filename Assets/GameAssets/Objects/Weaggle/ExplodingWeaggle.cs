﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ExplodingWeaggle : NetworkBehaviour
{
    [SerializeField]
    List<Renderer> _renderers = null;

    [SerializeField]
    Animator _vein = null;

    [SerializeField]
    float _detonationTime = 0;

    [SerializeField]
    GameObject explosion = null;    

    [SyncVar]
    private bool _chargingDetonation;

    [SyncVar]
    private float _detonationTimer;

    public bool chargingDetonation
    {
        get
        {
            return _chargingDetonation;
        }

        set
        {
            // Avoid setting dirty bits unnecessarily 
            if( _chargingDetonation != value)
                _chargingDetonation = value;
        }
    }



    private List<Color>  _originalColor;
    private readonly Color tintColor = Color.red;



    // Use this for initialization
    void Start()
    {
        _detonationTimer = _detonationTime;

        if( isClient )
        {
            _originalColor = new List<Color>( _renderers.Count + 1 );
            foreach( var renderer in _renderers )
            {
                _originalColor.Add( renderer.material.color );
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        if( isServer )
            UpdateServer();

        if( isClient )
            UpdateClient();
    }



    private void UpdateServer()
    {
        if( chargingDetonation && _detonationTimer>0 )
        {
            _detonationTimer -= Time.deltaTime;
            _detonationTimer = Mathf.Clamp( _detonationTimer, 0, _detonationTime );
        }
        else if( _detonationTimer<_detonationTime )
        {
            _detonationTimer += Time.deltaTime;
            _detonationTimer = Mathf.Clamp( _detonationTimer, 0, _detonationTime );
        }

        if( _detonationTimer <= 0 )
        {
            Detonate();
        }
    }



    private void UpdateClient()
    {
        if( _vein )
        {
            _vein.SetBool( "visible", chargingDetonation );
        }

        if( _detonationTimer < _detonationTime )
        {
            float t = (_detonationTime - _detonationTimer)/_detonationTime;
            for( int i = 0; i < _renderers.Count; i++ )
            {
                _renderers[i].material.color = Color.Lerp( _originalColor[i], tintColor, t );
            }
        }
    }



    [Server]
    void Detonate()
    {
        var explosionInstance =
            Instantiate( explosion, transform.position, Quaternion.identity ) 
            as GameObject;

        if( explosionInstance )
            NetworkServer.Spawn( explosionInstance );

        // Go Boom!!!
        Destroy( gameObject );        
    }
}
