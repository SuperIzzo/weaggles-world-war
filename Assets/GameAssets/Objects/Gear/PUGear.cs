﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    [RequireComponent( typeof( IPickable ) )]
    public class PUGear : MonoBehaviour 
    {
        public float resource;



        protected void Start()
        {
            GetComponent<IPickable>().EventPickup += OnPickup;
        }



        private void OnPickup( GameObject picker )
        {
            var spawn = picker.GetComponent<ISpawnableUnit>();

            if( spawn!=null  &&  spawn.spawner!=null )
            {
                spawn.spawner.AddResource( resource );
            }
        }
    }
}