﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class Shatter : NetworkBehaviour
    {
        public GameObject effect;
        private GameObject _effect;


        
        public override void OnStartClient()
        {
            if( effect )
            {
                _effect = Instantiate(
                    effect,
                    transform.position,
                    transform.rotation ) as GameObject;
                _effect.SetActive( false );
                _effect.AddComponent<DelayedDestruction>().destructionTime = 5;
            }
        }



        public override void OnNetworkDestroy()
        {
            if( isClient && _effect)
            {
                _effect.SetActive( true );
            }
        }
    }
}