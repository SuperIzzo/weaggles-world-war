﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                         PUShield.cs                         </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A shield pickup object.                </summary>
    /// <remarks>
    ///     Spawns a shield when picked up, will try to trigger
    ///     IAttachable.AttachTo() on the newly spawned object.
    ///     It will also assign it to the same team as the picker.
    ///     This component requires an IPickable and responds to 
    ///     EventPickup.
    /// </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    [RequireComponent( typeof( IPickable ) )]
    public class PUShield : NetworkBehaviour
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The shield object to be spawned when on pickup."         )]
        //--------------------------------------
        GameObject _shield;



        //--------------------------------------------------------------
        /// <summary> Unity Start callback.                   </summary>
        //--------------------------------------
        protected void Start()
        {
            GetComponent<IPickable>().EventPickup += OnPickup;
        }



        //--------------------------------------------------------------
        /// <summary> IPickable.EventPickup callback.         </summary>
        //--------------------------------------
        private void OnPickup( GameObject picker )
        {
            if( isServer )
            {
                GameObject shieldObj = Instantiate( _shield ) as GameObject;
                NetworkServer.Spawn( shieldObj );

                var attachable = shieldObj.GetComponent<IAttachable>();

                if( attachable!=null )
                {
                    attachable.AttachTo( picker );
                }

                Team team = picker.GetComponent<Team>();
                if( team )
                {
                    Team shieldTeam = shieldObj.GetComponent<Team>();
                    if( shieldTeam == null )
                    {
                        shieldObj.AddComponent<Team>();
                        shieldTeam = shieldObj.GetComponent<Team>();
                    }

                    shieldTeam.Assign( team );
                }
            }
        }
    }
}