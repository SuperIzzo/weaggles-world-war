﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class HitShield : NetworkBehaviour, IAttachable, IHealthListener
    {
        private Animator animator;

        // Use this for initialization
        void Start()
        {
            animator = GetComponent<Animator>();
        }


        public void OnHideAnimationEnd()
        {
            if( isServer )
                Destroy( gameObject );
        }


        [Server]
        public void OnHealthChanged( object sender, float current, float prev )
        {
            if( current<=0 )
            {
                if( animator )
                {
                    animator.SetTrigger( "Destroy" );
                }
            }
        }

        [Server]
        public void AttachTo( GameObject attachTarget )
        {
            var targetID = attachTarget.GetComponent<NetworkIdentity>();

            transform.parent = attachTarget.transform;
            transform.localPosition = Vector3.zero;               
             
            if( targetID )
                RpcAttach( targetID );

            GetComponent<Collider>().enabled = true;
        }


        [ClientRpc]
        private void RpcAttach( NetworkIdentity targetID )
        {
            if( targetID && targetID.gameObject )
            {
                transform.parent = targetID.gameObject.transform;
                transform.localPosition = Vector3.zero;
            }
        }
    }
}