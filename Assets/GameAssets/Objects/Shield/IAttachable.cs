﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;

    public interface IAttachable
    {
        void AttachTo( GameObject target );
    }
}