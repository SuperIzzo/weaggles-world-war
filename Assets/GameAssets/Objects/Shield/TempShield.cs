﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class TempShield : NetworkBehaviour, IAttachable
    {
        [SyncVar]        
        private float _durationCountdown;

        [SerializeField, SyncVar]
        private float _duration;

        public float blinkTime;
        private Animator animator;

        // Use this for initialization
        void Start()
        {
            animator = GetComponent<Animator>();
            _durationCountdown = _duration;
        }


        // Update is called once per frame
        void Update()
        {
            _durationCountdown -= Time.deltaTime;

            if( _durationCountdown>0 )
            {
                if( _durationCountdown < blinkTime )
                {
                    if( animator )
                    {
                        animator.SetTrigger( "Blink" );
                    }
                }
            }
            else
            {
                if( animator )
                {
                    animator.SetTrigger( "Destroy" );
                }
            }
        }


        public void OnHideAnimationEnd()
        {
            Destroy( gameObject );
        }


        [Server]
        public void AttachTo( GameObject attachTarget )
        {
            var targetID = attachTarget.GetComponent<NetworkIdentity>();

            transform.parent = attachTarget.transform;
            transform.localPosition = Vector3.zero;

            if( targetID )
                RpcAttach( targetID );

            GetComponent<Collider>().enabled = true;
        }


        [ClientRpc]
        private void RpcAttach( NetworkIdentity targetID )
        {
            transform.parent = targetID.transform;
            transform.localPosition = Vector3.zero;
        }
    }
}