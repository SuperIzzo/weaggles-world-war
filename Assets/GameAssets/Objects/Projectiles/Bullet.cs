﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class Bullet : NetworkBehaviour, IPoolable, IProjectileType
    {
        // Configuration
        public float speed          = 7.0f;
        public float damage         = 25.0f;
        public ParticleSystem sparkParticles;
        public TrailRenderer  trail = null;

        [SerializeField]
        AudioClip _audioClip;

        [SerializeField]
        float _volume = 1;

        [SerializeField]
        int  _priority = 128;


        private int frameSkip = 8;
        private int frameCounter = 0;

        private TrailRenderer  _trail = null;
        Rigidbody _rigidBody;

        [SerializeField, Range(0.1f,30)]
        float syncRate = 10;
        float syncTimer = 0;        


        // State
        private bool destroyed = false;
        private Collider[] _shooterColliders;

        /// <summary>
        /// Update this instance.
        /// </summary>
        void Awake()
        {
            _rigidBody = GetComponent<Rigidbody>();
        }


        [Server]
        public void OnPoolInit()
        {
            Reset();            
        }


        [Server]
        public void OnPoolRelease()
        {
            UnignoreShooter();
        }
        


        public override bool OnSerialize( NetworkWriter writer, bool initialState )
        {            
            if( initialState || (syncTimer<=0 && !destroyed) )
            {
                syncTimer = 1 / syncRate;

                if( !initialState )
                    writer.Write( true );

                writer.Write( transform.position );
                writer.Write( _rigidBody.velocity );
                return true;
            }
            else
            {
                writer.Write( false );
                return false;
            }
        }



        public override void OnDeserialize( NetworkReader reader, bool initialState )
        {
            if( isServer )
                return;

            bool update = true;

            if(!initialState)
                update = reader.ReadBoolean();

            if( update )
            {
                transform.position = reader.ReadVector3();
                _rigidBody.velocity = reader.ReadVector3();                
            }
        }



        private void Reset()
        {
            if( _trail )
            {
                _trail.Clear();
                _trail.time = 0;
            }

            Vector3 direction = transform.rotation * Vector3.forward;

            destroyed = false;
            _rigidBody.angularVelocity = Vector3.zero;
            _rigidBody.useGravity = false;
            _rigidBody.inertiaTensor = Vector3.one;
            _rigidBody.inertiaTensorRotation = Quaternion.identity;
            _rigidBody.Sleep();

            transform.localScale = Vector3.one * 0.2f;

            _rigidBody.velocity = direction * speed;

            CreateTrail();
        }



        private void CreateTrail()
        {
            //yield return new WaitForSeconds( 0.05f );

            if( isClient )
            {
                if( !_trail )
                {
                    _trail = Instantiate( trail );
                    _trail.transform.SetParent( transform, false );
                }
                else
                {
                    _trail.Clear();
                    _trail.time = 0.1f;
                }
            }
        }



        void Update()
        {
            if( isServer )
                UpdateServer();

            if( isClient )
                UpdateClient();            
        }



        private void UpdateServer()
        {
            syncTimer -= Time.deltaTime;
        }



        private void UpdateClient()
        {
            // There are only aestectics
            // We can skip a few frames
            if( ++frameCounter < frameSkip )
                return;
            
            frameCounter = 0;

            if( destroyed )
            {
                if( transform.localScale.x > 0 )
                {
                    transform.localScale -=
                        Vector3.one * Time.deltaTime * 0.06f * frameSkip;
                }
                else
                {
                    transform.localScale = Vector3.zero;
                }
            }
            else if( _rigidBody.velocity.magnitude > 0.001f )
            {
                transform.rotation = Quaternion.LookRotation( _rigidBody.velocity );
            }
        }



        /// <summary>
        /// 	Raises the trigger enter event.
        /// </summary>
        /// <remarks>
        /// 	Invoked by the Unity engine. There are two possible resolves - 
        /// 	if the bullet and the collider are from the same team the event is ignored
        /// 	otherwise the bullet rises OnDamge event on the collider and destroys itself.
        /// </remarks>
        void OnCollisionEnter( Collision col )
        {
            if( isServer )
                Hit( col.collider );
            else
                return;
        }



        [Server]
        void Hit( Collider col )
        {
            lock ( this )
            {                                
                if( !destroyed )
                {
                    // Get our team
                    Team team = GetComponent<Team>();

                    // Don't hit allies
                    if( team == null || !team.IsAlly( col ) )
                    {
                        // Damage all damagable components on the collider
                        Health health = col.GetComponent<Health>();
                        if( health )
                        {
                            health.current -= damage;
                        }
                    }

                    destroyed = true;
                    _rigidBody.useGravity = true;
                    _rigidBody.AddForce( Random.rotation * Vector3.forward * (1 + Random.value * 3) * 100 );                    

                    Pool.Release( gameObject );

                    RpcOnBulletHit();
                }
            }
        }


        [ClientRpc]
        private void RpcOnBulletHit()
        {            
            if( sparkParticles )
            {
                sparkParticles.Play();
                sparkParticles.transform.parent = null;
                sparkParticles.transform.position = transform.position;
            }

            destroyed = true;
            _rigidBody.useGravity = true;

            
            if( _trail )
            {
                _trail.Clear();
                _trail.time = 0;
            }
        }



        [Server]
        GameObject IProjectileType.ShootNew( 
            Component shooter, 
            Vector3 position, 
            Quaternion rotation )
        {
            GameObject  bullet = Pool.Instantiate( gameObject, position, rotation );
            NetworkServer.Spawn( bullet );            

            // If our projectile has a team, assign it to our team
            Team team = shooter.GetComponent<Team>();
            if( team != null )
            {
                Team bulletTeam = bullet.GetComponent<Team>();
                if( bulletTeam != null )
                {
                    bulletTeam.Assign( team );
                }
            }

            Bullet bulletComponent = bullet.GetComponent<Bullet>();
            bulletComponent.IgnoreShooter( shooter );
            bulletComponent.ShootSelf( position, rotation );

            return bullet;
        }



        private void ShootSelf( Vector3 position, Quaternion rotation )
        {
            RpcOnBulletShot( position, rotation );
        }



        [ClientRpc]
        private void RpcOnBulletShot( Vector3 position, Quaternion rotation )
        {
            transform.position = position;
            transform.rotation = rotation;
            PlayShootSFX();
            Reset();
        }



        private void PlayShootSFX()
        {

            if( _audioClip != null )
            {
                AudioUtilities.PlaySFXAtPoint(
                    _audioClip,
                    transform.position,
                    _volume,
                    _priority );
            }
        }



        private void IgnoreShooter( Component shooter )
        {
            var bulletCollider = GetComponent<Collider>();
            if( bulletCollider )
            {
                _shooterColliders = shooter.GetComponentsInChildren<Collider>();

                foreach( var shooterCollider in _shooterColliders )
                {
                    Physics.IgnoreCollision( bulletCollider, shooterCollider );
                }
            }
        }



        private void UnignoreShooter()
        {
            if( _shooterColliders == null )
                return;

            var bulletCollider = GetComponent<Collider>();
            if( bulletCollider )
            {
                foreach( var shooterCollider in _shooterColliders )
                {
                    if( shooterCollider )
                    {
                        Physics.IgnoreCollision(
                            bulletCollider,
                            shooterCollider,
                            false );
                    }
                }
            }
        }


    }
}