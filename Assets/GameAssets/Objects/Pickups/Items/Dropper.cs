using UnityEngine;
using UnityEngine.Networking;

public class Dropper : Item
{
	public GameObject drop;

	public override void Use (GameObject user)
	{
		Vector3 pos = user.transform.position + user.transform.rotation * Vector3.back;

		GameObject obj = Instantiate( drop, pos, Quaternion.identity ) as GameObject;
        NetworkServer.Spawn( obj );
	}
}

