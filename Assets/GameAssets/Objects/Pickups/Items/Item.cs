using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField]    string _itemName;
    [SerializeField]    Sprite _itemIcon;

    public virtual string itemName { get { return _itemName; } }
    public virtual Sprite itemIcon { get { return _itemIcon; } }
    public virtual int itemID { get { return itemName.GetHashCode(); } }

    public virtual void Use( GameObject user )
	{
	}
}

