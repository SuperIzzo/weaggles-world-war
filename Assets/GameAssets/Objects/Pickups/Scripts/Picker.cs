﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                          Picker.cs                          </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    23-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A basic picker component.              </summary>
    /// <remarks> 
    ///     <c>Picker</c> will try to pick up any <c>IPickable</c>
    ///     object it triggers or collides with. Only the first 
    ///     IPickable is notified.
    /// </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    public class Picker : NetworkBehaviour
    {
        //--------------------------------------------------------------
        /// <summary> Unity OnTriggerEnter callback.          </summary>
        /// <remarks>
        ///     Only called on the server.
        /// </remarks>
        //--------------------------------------
        [ServerCallback]
        protected void OnTriggerEnter( Collider col )
        {
            var pickable = col.transform.GetComponent<IPickable>();
            if( pickable != null )
            {
                pickable.OnPickup( gameObject );
            }
        }



        //--------------------------------------------------------------
        /// <summary> Unity OnCollisionEnter callback.        </summary>
        /// <remarks>
        ///     Only called on the server.
        /// </remarks>
        //--------------------------------------
        [ServerCallback]
        protected void OnCollisionEnter( Collision col )
        {
            var pickable = col.transform.GetComponent<IPickable>();
            if( pickable != null )
            {
                pickable.OnPickup( gameObject );
            }
        }
    }
}