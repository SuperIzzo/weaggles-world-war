﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                          Pickup.cs                          </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    18-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A basic pickup implementation.         </summary>
    /// <remarks> 
    ///     The pickup component allows the game object to receive
    ///     and respond to OnPickup callbacks. The behaviour of the
    ///     pickup is defined by other components. After the event
    ///     has been dispatched the pickup object is destroyed.
    /// </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    public class Pickup : MonoBehaviour, IPickable
    {
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "An audio clip to play then the object is picked up."     )]
        //--------------------------------------
        private AudioClip _pickupSound;



        //--------------------------------------------------------------
        /// <summary> Pickup event, dispatched when the object 
        ///           is picked up.                           </summary>
        /// <remarks>
        ///     Subscribe to this event to define specific behaviours.
        /// </remarks>
        //--------------------------------------
        public event PickupHandler EventPickup;



        //--------------------------------------------------------------
        /// <summary> Fires a pickup event.  </summary>
        /// <param name="picker"> The object that picks up. </param>
        /// <remarks>
        ///     This method is intended to be called from pickers to
        ///     signal a pickup. After calling this method, EventPickup
        ///     will be dispatched, the pickup sound will play and the
        ///     pickup game object is destroyed.
        /// </remarks>
        //--------------------------------------
        public virtual void OnPickup( GameObject picker )
        {
            if( EventPickup!=null )
                EventPickup( picker );

            if( _pickupSound )
                AudioUtilities.PlaySFXAtPoint( _pickupSound, transform.position );

            Destroy( gameObject );
        }
    }
}