﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    [RequireComponent( typeof( IPickable ) )]
    public class PUItem : MonoBehaviour
    {
        public Item item;
        public int  number;



        protected void Start()
        {
            GetComponent<IPickable>().EventPickup += OnPickup;
        }



        private void OnPickup( GameObject picker )
        {
            ItemSlot itemSlot = picker.GetComponent<ItemSlot>();

            if( itemSlot && itemSlot.isEmpty )
            {
                itemSlot.SetItem( item, number );
            }
        }
    }
}