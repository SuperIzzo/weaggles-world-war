/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                        IPickable.cs                         </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    19-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;



    //--------------------------------------------------------------
    /// <summary> Pickup handling method reference.       </summary>
    //--------------------------------------
    public delegate void PickupHandler( GameObject picker );



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  A generic pickup interface.            </summary>
    /// <remarks>
    ///     You should only have one IPickable component 
    ///     per GameObject.
    ///                                                   </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    public interface IPickable
    {
        //--------------------------------------------------------------
        /// <summary> Pickup event, dispatched when the object 
        ///           is picked up.                           </summary>
        /// <remarks>
        ///     Subscribe to this event to define specific behaviours.
        ///                                                   </remarks>
        //--------------------------------------
        event PickupHandler EventPickup;



        //--------------------------------------------------------------
        /// <summary> Signals a pickup.                       </summary>
        /// <param name="picker"> 
        ///     The object that picks the IPickable.
        ///     Ideally also the one calling.
        /// </param>
        /// <remarks>
        ///     This method is intended to be called from pickers to
        ///     signal a pickup. It should serve as a trigger to 
        ///     <c>EventPickup</c>, although that does not have to be
        ///     immediately or unconditionally.
        ///                                                   </remarks>
        //--------------------------------------
        void OnPickup( GameObject picker );
    }
}

