﻿namespace Izzo.WeagglesWorldWar
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Networking;

    public class SpeedBuffObject : NetworkBehaviour
    {
        public TankMotor motor  { get; set; }
        public Timer duration   { get; set; }
        public float addedSpeed { get; set; }

        private Buff _buff;


        public override void OnStartServer()
        {
            if( motor )
            {
                _buff = new SpeedBuff( addedSpeed );
                motor.AddBuff( _buff );
                duration.Reset();

                var motorNetID = motor.GetComponent<NetworkIdentity>();

                if( motorNetID )
                {
                    transform.SetParent( motor.transform, false );
                    StartCoroutine( DelayedRpcAttachToMotor( motorNetID ) );                    
                }
            }
        }


        private IEnumerator DelayedRpcAttachToMotor( NetworkIdentity motorNetID )
        {
            // apparently we need to wait a frame
            yield return null;
            RpcAttachToMotor( motorNetID );
        }


        [ClientRpc]
        void RpcAttachToMotor( NetworkIdentity motorNetID )
        {
            transform.SetParent( motorNetID.transform, false );
            transform.localPosition = Vector3.zero;
        }


        
        [ServerCallback]
        void Update()
        {
            if( !isServer )
                return;

            duration.Step( Time.deltaTime );            

            if( !duration.isRunning && motor )
            {
                motor.RemoveBuff( _buff );
                motor = null;

                // Destroy ourselves
                Destroy( gameObject );
            }
        }
    }
}