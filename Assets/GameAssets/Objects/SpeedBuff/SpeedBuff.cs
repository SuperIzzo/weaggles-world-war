﻿namespace Izzo.WeagglesWorldWar
{
    public class SpeedBuff : Buff
    {
        private float _addedSpeed;

        public float addedSpeed { get { return _addedSpeed; } }

        public SpeedBuff( float addedSpeed )
        {
            _addedSpeed = addedSpeed;
        }        
    }
}