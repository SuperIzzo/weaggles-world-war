﻿namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;

    [RequireComponent( typeof( IPickable ) )]
    public class PUSpeed : MonoBehaviour 
    {
        public Timer duration;
        public float addedSpeed;

        public SpeedBuffObject buff;



        protected void Start()
        {
            GetComponent<IPickable>().EventPickup += OnPickup;
        }



        private void OnPickup( GameObject picker )
        {
            var speedBuff = Instantiate( buff ) as SpeedBuffObject;            
            speedBuff.motor = picker.GetComponent<TankMotor>();
            speedBuff.addedSpeed = addedSpeed;
            speedBuff.duration = duration;
            NetworkServer.Spawn( speedBuff.gameObject );
        }
    }
}