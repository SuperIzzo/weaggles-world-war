﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                       UnitSpawner.cs                        </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    07-Oct-2014                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;
    using System.Collections.Generic;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  Unit spawner.                          </summary>
    /// <remarks>  Deals with resources and conditions.   </remarks>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 
    public class UnitSpawner : NetworkBehaviour, IUnitSpawner
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The spawn position relative to the spawner."             )]
        //-------------------------------------- 
        Vector3 _offset;


        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "Check to make the spawner spawn automatically."          )]
        //-------------------------------------- 
        bool _automaticSpawn = true;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "Check to allow the spawner to spawn players."            )]
        //-------------------------------------- 
        bool _spawnsPlayers   = false;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "The spawn cooldown time (in seconds)."                   )]
        //-------------------------------------- 
        Timer _spawnTimer = new Timer( 5.0f );



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "A list of resources, each pays the full unit cost."      )]
        //-------------------------------------- 
        Component[] _resources;



        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "Objects to spawn, must have an IBaseSpawnable component.")]
        //-------------------------------------- 
        List<GameObject> _spawnObjects;
        #endregion
        //......................................



        //--------------------------------------------------------------
        /// <summary>  Gets whether this spawner can spawn players. </summary>
        //--------------------------------------
        public bool spawnsPlayers{ get { return _spawnsPlayers; } }


        //--------------------------------------------------------------
        /// <summary>  Holds a reference to our team.         </summary>
        //--------------------------------------
        private Team _team;


        //..............................................................
        #region               //  METHODS  //        
        //--------------------------------------------------------------
        /// <summary>  Initiates the spawner.                 </summary>
        //--------------------------------------
        protected void Start()
        {
            _spawnObjects.RemoveAll( 
                spawnable => spawnable.GetComponent<ISpawnableUnit>()==null );

            _team = GetComponent<Team>();
        }



        //--------------------------------------------------------------
        /// <summary>  Handles automatic spawning.            </summary>
        //--------------------------------------
        protected void Update()
        {
            if( isServer )
            {
                _spawnTimer.Step( Time.deltaTime );

                if( _automaticSpawn )
                {
                    Spawn();
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary>  Returns wheter this spawner can spawn the given
        ///            <c>ISpawnableUnit</c>.                 </summary>
        /// <param name="obj">
        ///              An <c>ISpawnableUnit</c> to be checked, 
        ///              if null is provided the method will instead 
        ///              check whether the spawner can spawn its 
        ///              cheapest unit                          </param>
        /// <returns> return <c>true</c> the spawner has enough
        ///           resources to spawn <c>obj</c>; 
        ///           <c>false</c> otherwise.                 </returns>
        //--------------------------------------
        public bool CanSpawn( ISpawnableUnit obj = null )
        {
            if( obj==null )
            {
                obj = GetCheapestUnit();
            }

            return CanPay( obj );
        }



        //--------------------------------------------------------------
        /// <summary>Spawns a random unit from the spawn list.</summary>
        /// <remarks>
        ///     The method will fail silently returning <c>null</c> if
        ///     the internal spawn cooldown timer is still running or
        ///     there are not enough resources to pay for the randomly 
        ///     chosen unit. On succesfull spawn the spawn cooldiwn
        ///     timer will be reset.                          </remarks>
        //--------------------------------------
        [Server]
        public ISpawnableUnit Spawn()
        {
            if( _spawnTimer.isRunning )
            {
                return null;
            }

            int objectIdx = UnityEngine.Random.Range( 0, _spawnObjects.Count );
            GameObject objectType = _spawnObjects[objectIdx];

            if( PayForObject( objectType ) )
            {
                Vector3 globalSpaceOffset = transform.rotation * _offset;

                var instance = Instantiate(    
                            objectType,
                            transform.position + globalSpaceOffset,
                            transform.rotation ) as GameObject;

                NetworkServer.Spawn( instance );

                var spawnable = instance.GetComponent<ISpawnableUnit>();
                if( spawnable != null )
                {
                    spawnable.OnSpawn( this );
                    spawnable.EventDestroyed += OnSpawnedObjectDestroyed;
                }

                if( _team )
                {
                    var spawnTeam = instance.GetComponent<Team>();
                    if( spawnTeam )
                    {
                        spawnTeam.Assign( _team );
                    }
                }

                _spawnTimer.Reset();

                return spawnable;
            }

            return null;
        }



        //--------------------------------------------------------------
        /// <summary>  Adds resource to all <c>IResource</c>-s. </summary>
        //--------------------------------------
        [Server]
        public void AddResource( float amount )
        {
            foreach( var resourceComponent in _resources )
            {
                var resource = resourceComponent as IResource;
                if( resource != null )
                {
                    resource.currentResource += amount;
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary> Returns the cheapest unit in terms of resource.</summary>
        //--------------------------------------
        public ISpawnableUnit GetCheapestUnit()
        {
            ISpawnableUnit cheapest = null;

            foreach( GameObject go in _spawnObjects )
            {
                ISpawnableUnit spawnable = go.GetComponent<ISpawnableUnit>();

                if( cheapest==null || 
                    (spawnable!=null && 
                    cheapest.cost < spawnable.cost ) )

                    cheapest = spawnable;
            }

            return cheapest;
        }




        //--------------------------------------------------------------
        /// <summary>  Handles the dealocation of upkeep
        ///            when <c>ISpawnableUnit</c> are destroyed. </summary>
        //--------------------------------------
        private void OnSpawnedObjectDestroyed( ISpawnableUnit obj )
        {
            foreach( var resourceComponent in _resources )
            {
                var resource = resourceComponent as IResource;
                if( resource != null )
                {
                    resource.currentUpkeep -= obj.upkeep;
                }
            }
        }



        //--------------------------------------------------------------
        /// <summary> Deducts resource from all <c>IResource</c>-s. </summary>
        //--------------------------------------
        private bool PayForObject( GameObject objectType )
        {
            var spawnable = objectType.GetComponent<ISpawnableUnit>();

            // Non-spawnable objects cost nothing
            if( spawnable == null )
                return true;

            if( CanPay( spawnable ) )
            {
                foreach( var resourceComponent in _resources )
                {
                    var resource = resourceComponent as IResource;
                    if( resource != null )
                    {
                        resource.currentUpkeep += spawnable.upkeep;
                        resource.currentResource -= spawnable.cost;
                    }
                }

                return true;
            }

            return false;
        }



        //--------------------------------------------------------------
        /// <summary> Returns whether the spawnable can be paid. </summary>
        //--------------------------------------
        private bool CanPay( ISpawnableUnit spawnable )
        {
            bool canPay = true;

            foreach( Component resourceComponent in _resources )
            {
                var resource = resourceComponent as IResource;

                if( resource != null )
                {
                    canPay &= resource.currentUpkeep + spawnable.upkeep <= resource.upkeepLimit;
                    canPay &= spawnable.cost < resource.currentResource;
                }
            }

            return canPay;
        }
        #endregion
        //......................................
    }
}