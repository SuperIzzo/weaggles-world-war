﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                        IResource.cs                         </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    25-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  An abstract spawn resource interface.  </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public interface IResource
    {
        //--------------------------------------------------------------
        /// <summary>  The upper reosurce cap.                </summary>
        //--------------------------------------
        float resourceLimit { get; set; }

        //--------------------------------------------------------------
        /// <summary>  Current resource amount.               </summary>
        //--------------------------------------
        float currentResource { get; set; }

        //--------------------------------------------------------------
        /// <summary>  Upper upkeep limit.                    </summary>
        //--------------------------------------
        float upkeepLimit { get; set; }

        //--------------------------------------------------------------
        /// <summary>  Current upkeep.                        </summary>
        //--------------------------------------
        float currentUpkeep { get; set; }
    }
}