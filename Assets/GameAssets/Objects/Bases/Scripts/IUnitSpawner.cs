﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                       IUnitSpawner.cs                       </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    25-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>  Abstract interface for all unit spawners.  </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public interface IUnitSpawner
    {
        //--------------------------------------------------------------
        /// <summary>  Returns whether the spawner can spawn the
        ///            specified ISpawnableUnit.               <summary>
        /// <remarks>  Must also accept null as an argument in which 
        ///            case it must return whether it can spawn
        ///            anything at all.                       </remarks>
        //--------------------------------------     
        bool CanSpawn( ISpawnableUnit obj = null );



        //--------------------------------------------------------------
        /// <summary> Spawns a unit.                          </summary>
        //--------------------------------------
        ISpawnableUnit Spawn();



        //--------------------------------------------------------------
        /// <summary> Adds an amount to the resorces.         </summary>
        // TODO: Replace with compound resource implementation in UnitSpawner
        //--------------------------------------
        void AddResource( float amount );



        //--------------------------------------------------------------
        /// <summary> Returns whether the spawner can spawn players. </summary>
        //--------------------------------------
        bool spawnsPlayers { get; }
    }
}