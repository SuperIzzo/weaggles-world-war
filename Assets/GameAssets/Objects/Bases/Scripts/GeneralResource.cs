﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                     GeneralResource.cs                      </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    25-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>    A basic resource system that 
    ///              maintains its own state.             </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class GeneralResource : NetworkBehaviour, IResource
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The current resource."                                   )]
        //--------------------------------------
        float _currentResource = 0.0f;


        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The resource cap, beyond which current cannot increase." )]
        //--------------------------------------
        float _resourceLimit = 0.0f;    


        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The current upkeep."                                     )]
        //--------------------------------------
        float _currentUpkeep = 0.0f;


        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The upkeep cap, beyond which current cannot increase."   )]
        //--------------------------------------
        float _upkeepLimit = 4.0f;
        #endregion
        //......................................   



        //..............................................................
        #region            //  PUBLIC PROPERTIES  //
        //--------------------------------------------------------------
        /// <summary> Gets and sets the current resource.     </summary>
        //--------------------------------------
        public float currentResource
        {
            get { return _currentResource; }
            set { _currentResource = value; }
        }


        //--------------------------------------------------------------
        /// <summary> Gets and sets the resource limit.       </summary>
        //--------------------------------------
        public float resourceLimit
        {
            get { return _resourceLimit;  }
            set { _resourceLimit = value; }
        }


        //--------------------------------------------------------------
        /// <summary> Gets and sets the current upkeep.       </summary>
        //--------------------------------------
        float IResource.currentUpkeep
        {
            get { return _currentUpkeep; }
            set { _currentUpkeep = value; }
        }


        //--------------------------------------------------------------
        /// <summary> Gets and sets the upkeep limit.         </summary>
        //--------------------------------------
        float IResource.upkeepLimit
        {
            get { return _upkeepLimit;  }
            set { _upkeepLimit = value; }
        }
        #endregion
        //......................................
    }
}