﻿/** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **\
|*                                                                            *|
 * <file>                      HealthResource.cs                      </file> * 
 *                                                                            * 
 * <copyright>                                                                * 
 *   Copyright (C) 2015  Hristoz Stefanov - All Rights Reserved               * 
 *                                                                            * 
 *   Unauthorized copying of this file, via any medium is strictly prohibited * 
 *   Proprietary and confidential                                             * 
 *                                                               </copyright> * 
 *                                                                            * 
 * <author>  Hristoz Stefanov                                       </author> * 
 * <date>    25-Jan-2016                                              </date> * 
|*                                                                            *|
\** -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- **/
namespace Izzo.WeagglesWorldWar
{
    using UnityEngine;
    using UnityEngine.Networking;



    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    /// <summary>   A health based resource system.       </summary>
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    public class HealthResource : NetworkBehaviour, IResource
    {
        //..............................................................
        #region            //  INSPECTOR SETTINGS  //
        //--------------------------------------------------------------
        [SerializeField, Tooltip
        (   "A Health component to be used as a resource."            )]
        //--------------------------------------
        Health _health;


        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The current upkeep."                                     )]
        //--------------------------------------
        float _currentUpkeep;


        //--------------------------------------------------------------
        [SerializeField, SyncVar, Tooltip
        (   "The upkeep cap, beyond which current cannot increase."   )]
        //--------------------------------------
        float _upkeepLimit;
        #endregion
        //......................................



        //..............................................................
        #region            //  PUBLIC PROPERTIES  //
        //--------------------------------------------------------------
        /// <summary>  Gets and sets the current resource
        ///            from a health component.               </summary>
        /// <remarks>  This property maps to Health.current and changes
        ///             may cause health events to be fired.   </remarks>
        //--------------------------------------
        public float currentResource
        {
            get { return _health.current;  }
            set { _health.current = value; }
        }


        //--------------------------------------------------------------
        /// <summary>  Gets and sets the resource limit
        ///            from a health component.               </summary>
        /// <remarks>  This property maps to Health.max and changes
        ///            may cause health events to be fired.   </remarks>
        //--------------------------------------
        public float resourceLimit
        {
            get { return _health.max; }
            set { _health.max = value; }
        }


        //--------------------------------------------------------------
        /// <summary> Gets and sets the current upkeep.       </summary>
        //--------------------------------------
        public float currentUpkeep
        {
            get { return _currentUpkeep;  }
            set { _currentUpkeep = value; }
        }


        //--------------------------------------------------------------
        /// <summary> Gets and sets the upkeep limit.         </summary>
        //--------------------------------------        
        public float upkeepLimit
        {
            get { return _upkeepLimit; }
            set { _upkeepLimit = value; }
        }
        #endregion
        //......................................



        //--------------------------------------------------------------
        /// <summary> Initialises us. Grab Health if none specified. </summary>
        //--------------------------------------
        protected void Awake()
        {
            if( !_health )
                _health = GetComponent<Health>();
        }
    }
}